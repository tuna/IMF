subroutine skip_header(unit)
  integer(4) :: unit
  character*256 :: buf
  DO
    READ(unit,'(a)') buf
    select case (buf(1:1))
    case ('%')
      !write(*,*) "SKIP: ", buf
      cycle
    case ('#')
      !write(*,*) "SKIP: ", buf
      cycle
    case ('!')
      !write(*,*) "SKIP: ", buf
      cycle
    case DEFAULT
      !write(*,*) "REWIND:", buf
      backspace(unit)
      exit
    end select
  END DO
end subroutine

subroutine imf_load_vector(filename,N,X)

  use imf_util
  character*256 :: filename
  integer(4),parameter :: unit = 18
  integer(kind=kint) :: i,N
  real (kind=kreal) :: X(N)
  real (kind=kreal) :: v
  open(unit,file=filename, status='old')
  call skip_header(unit)
  i=1
  X(:)=0.0d0
  !DO WHILE (.NOT. EOF(1234))
  DO
    READ(unit,*) X(i)
    i=i+1
    IF(IS_IOSTAT_END(unit)) exit
  END DO
  close(unit)

end subroutine imf_load_vector

subroutine imf_load_csr(filename,A)

  use imf_util
  implicit none
  type (imf_csr) :: A
  integer(4),parameter :: unit = 18
  character*256 :: buf
  character*256 :: filename
  integer(kind=kint) :: row,col,NZ,i,NDOF,NDOF2
  WRITE(*,*) "CSR matrix read from ", filename
  open(unit,file=filename, status='old')
  call skip_header(unit)
  READ(unit,*) row,col,NZ, NDOF

  A%N=row
  A%NP = NZ
  A%NDOF = NDOF
  NDOF2=NDOF*NDOF

  allocate(A%index(0:row))
  allocate(A%item(NZ))
  allocate(A%A(NDOF2*NZ))

  A%A(:)=0.0d0
  A%index(:)=0
  A%item(:)=0
  call skip_header(unit)
  do i=0,row
    read(unit,*) A%index(i)
  end do
  !write(*,*)"Index loaded."
  call skip_header(unit)
  do i=1,NZ
    read(unit,*) A%item(i)
  end do
  !write(*,*)"Item loaded"
  call skip_header(unit)
  do i=1,NDOF2*NZ
    read(unit,*) A%A(i)
  end do
  !write(*,*)"Value loaded"
  close(unit)

end subroutine imf_load_csr

subroutine imf_load_coo(filename,A)

  use imf_util
  implicit none
  type (imf_coo) :: A
  integer(4),parameter :: unit = 18
  character*256 :: filename
  character*256 buf
  integer(kind=kint) :: row,col,NZ,i,NDOF,NDOF2

  WRITE(*,*) "COO matrix read from ", filename
  open(unit,file=filename, status='old')
  call skip_header(unit)
  READ(buf,*) row,col,NZ

  NDOF=1
  A%ROW  = row
  A%COL  = col
  A%NP   = NZ
  A%NDOF = 1
  NDOF2  = NDOF*NDOF
  allocate(A%I(1:NDOF2*NZ))
  allocate(A%J(1:NDOF2*NZ))
  allocate(A%A(1:NDOF2*NZ))
  A%A(:)=0.0d0
  A%I(:)=0
  A%J(:)=0

  call skip_header(unit)
  do i=1,NZ
    read(unit,*) A%I(i),A%J(i),A%A(i)
  end do
  close(unit)

  write(*,*) "COO Read OK"
  do i=1,NZ
    !write(*,*) A%I(i),A%J(i),A%A(i)
  end do

end subroutine imf_load_coo


subroutine imf_print_matrixLU(hecMAT)

  use imf_util
  implicit none
  type (imf_smatrix) :: hecMAT
  integer :: i, j, i0, j0, idof, jdof, ii, jj,c
  integer :: idx, idxD, idxL, idxU, idxL0, idxU0
  integer :: n, np, ndof, ndof2, nnz
  real(kind=kreal),allocatable :: zz(:)
  character :: lineFormat*16, wrinum*8
  Write(wrinum,"(I0)") hecMAT%N*hecMAT%NDOF
  lineFormat = "(" //trim(wrinum)// "e9.2e1)"

  allocate(zz(hecMAT%N * hecMAT%NDOF))
  zz(:)=0
  ndof = hecMAT%NDOF
  ndof2=ndof*ndof
  n = hecMAT%N
  idxD = 0
  do i = 1, n
    i0 = (i-1)*ndof
    do idof = 1, ndof
      c=0
      ii = i0 + idof
      ! Lower
      do j = hecMAT%indexL(i-1)+1,hecMAT%indexL(i)
        j0 = (hecMAT%itemL(j)-1)*ndof
        idxL0 = (j-1)*ndof2 + (idof-1)*ndof
        do jdof = 1, ndof
          jj = j0 + jdof
          idxL = idxL0+jdof
          zz(jj) = hecMAT%AL(idxL)
        end do
      end do
      ! Diagonal
      j0 = i0
      do jdof = 1, ndof
        jj = j0 + jdof
        idxD = idxD + 1
        c = c+1
        zz(jj) = hecMAT%D(idxD)
      end do
      ! Upper
      do j = hecMAT%indexU(i-1)+1,hecMAT%indexU(i)
        j0 = (hecMAT%itemU(j)-1)*ndof
        idxU0 = (j-1)*ndof2 + (idof-1)*ndof
        do jdof = 1, ndof
          jj = j0 + jdof
          idxU = idxU0 + jdof
          c = c+1
          zz(jj) = hecMAT%AU(idxU)
        end do
      end do
      write(*,lineFormat) zz
    end do
  end do



  !   do i = 1, NP
  !     i0 = (i-1)*ndof
  !     do idof = 1, ndof
  !       ! Lower
  !       do j = hecMAT%indexL(i-1)+1,hecMAT%indexL(i)
  !         j0 = (hecMAT%itemL(j)-1)*ndof
  !         do jdof = 1, ndof
  !           jj = j0 + jdof
  !           write(*,"(I0)") jj
  !         end do
  !       end do
  !       ! Diagonal
  !       j0 = i0
  !       do jdof = 1, ndof
  !         jj = j0 + jdof
  !         write(iDump,"(I0)") jj
  !       end do
  !       ! Upper
  !       do j = hecMAT%indexU(i-1)+1,hecMAT%indexU(i)
  !         j0 = (hecMAT%itemU(j)-1)*ndof
  !         do jdof = 1, ndof
  !           jj = j0 + jdof
  !           write(iDump,"(I0)") jj
  !         end do
  !       end do
  !     end do
  !     write(*, writer)zz
  !   end do

end subroutine imf_print_matrixLU

subroutine imf_print_matrixL(A)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  integer(kind=kint) :: i,j,in,jS,jE
  real(kind=kreal),allocatable :: zz(:)
  character :: writer*16, wrinum*8

  Write(wrinum,"(I0)") A%N+1
  writer = "(1p" //trim(wrinum)// "E10.2)"
  allocate(zz(A%N))
  do i = 1,A%N
    zz(:)=0.0d0
    zz(i)=A%D(i)
    jS= A%indexL(i-1) + 1
    jE= A%indexL(i  )
    do j= jS, jE
      in = A%itemL(j)
      zz(in)=A%AL(j)
    end do
    write(*, writer)zz
  end do

end subroutine imf_print_matrixL


subroutine imf_print_matrixU(A)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  integer(kind=kint) :: i,j,in,jS,jE
  real(kind=kreal),allocatable :: zz(:)
  character :: writer*16, wrinum*8

  Write(wrinum,"(I0)") A%N+1
  writer = "(1p" //trim(wrinum)// "E10.2)"
  allocate(zz(A%N))
  do i = 1,A%N
    zz(:)=0.0d0
    zz(i)=A%D(i)
    jS= A%indexU(i-1) + 1
    jE= A%indexU(i  )
    do j= jS, jE
      in = A%itemU(j)
      zz(in)=A%AU(j)
    end do
    write(*, writer)zz
  end do


end subroutine imf_print_matrixU


subroutine imf_print_profileL(A)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  integer(kind=kint) :: i,j,in,jS,jE
  character :: writer*16, wrinum*8
  character, allocatable :: p(:,:)
  Allocate(p(A%N+5,A%N+5))

  Write(wrinum,"(I0)") A%N+1
  writer = "(1p" //trim(wrinum)// "I1)"

  do i = 1,A%N
    p(i,:)=" "
    p(i,i)="X"
    jS= A%indexL(i-1) + 1
    jE= A%indexL(i  )
    do j= jS, jE
      in = A%itemL(j)
      p(i,in)="X"
    end do
    write(*,*)p(i,:)
  end do

end subroutine imf_print_profileL

subroutine imf_print_profileU(A)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  integer(kind=kint) :: i,j,in,jS,jE
  character :: writer*16, wrinum*8
  character, allocatable :: p(:,:)
  Allocate(p(A%N+5,A%N+5))

  Write(wrinum,"(I0)") A%N+1
  writer = "(1p" //trim(wrinum)// "I1)"

  do i = 1,A%N
    p(i,:)=" "
    p(i,i)="X"
    jS= A%indexU(i-1) + 1
    jE= A%indexU(i  )
    do j= jS, jE
      in = A%itemU(j)
      p(i,in)="X"
    end do
    write(*,*)p(i,:)
  end do        

end subroutine imf_print_profileU

subroutine imf_print_profile(A)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  integer(kind=kint) :: i,j,in,jS,jE
  character :: writer*16, wrinum*8
  character, allocatable :: p(:,:)
  Allocate(p(A%N+5,A%N+5))

  Write(wrinum,"(I0)") A%N+1
  writer = "(1p" //trim(wrinum)// "I1)"

  do i = 1,A%N
    p(i,:)=" "
    jS= A%indexL(i-1) + 1
    jE= A%indexL(i  )
    do j= jS, jE
      in = A%itemL(j)
      p(i,in)="X"
    end do
    jS= A%indexU(i-1) + 1
    jE= A%indexU(i  )
    do j= jS, jE
      in = A%itemU(j)
      p(i,in)="X"
    end do

    if (A%D(i) /= 0) then
      p(i,i)="X"
    else
      p(i,i)="-"
    end if

    write(*,*)p(i,:)
  end do        

end subroutine imf_print_profile





subroutine imf_load_matrix_from_file(filename,A)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  character*256 :: filename
  integer(kind=kint) :: R,NZ,NDOF,i
  NDOF=1  
  open(18,file=filename, status='old')
  read (18,*) R,NZ
  call imf_init_matrix(A,R,NDOF,NZ)
  read(18,*)
  do i=0,R
    read(18,*) A%indexL(i)
  end do
  read(18,*)
  do i=1,NZ
    read(18,*) A%itemL(i)
  end do
  read(18,*)
  do i=1,R
    read(18,*) A%D(i)
  end do
  read(18,*)
  do i=1,NZ
    read(18,*) A%AL(i)
  end do
  close(18)

end subroutine imf_load_matrix_from_file



subroutine imf_dump_matrixLU(filename,hecMAT)

  use imf_util
  implicit none
  type (imf_smatrix) :: hecMAT
  character*256 :: filename
  integer :: i, j, i0, j0, idof, jdof, ii, jj,c
  integer :: idx, idxD, idxL, idxU, idxL0, idxU0
  integer :: n, np, ndof, ndof2, nnz
  real(kind=kreal),allocatable :: zz(:)
  character :: lineFormat*16, wrinum*8
  Write(wrinum,"(I0)") hecMAT%N*hecMAT%NDOF
  lineFormat = "(" //trim(wrinum)// "(e12.5,','))"

  open(17, file=filename, status='replace')

  allocate(zz(hecMAT%N * hecMAT%NDOF))
  zz(:)=0
  ndof = hecMAT%NDOF
  ndof2=ndof*ndof
  n = hecMAT%N
  idxD = 0
  do i = 1, n
    i0 = (i-1)*ndof
    zz(:)=0
    do idof = 1, ndof
      ii = i0 + idof
      ! Lower
      do j = hecMAT%indexL(i-1)+1,hecMAT%indexL(i)
        j0 = (hecMAT%itemL(j)-1)*ndof
        idxL0 = (j-1)*ndof2 + (idof-1)*ndof
        do jdof = 1, ndof
          jj = j0 + jdof
          idxL = idxL0 + jdof
          zz(jj) = hecMAT%AL(idxL)
        end do
      end do
      ! Diagonal
      j0 = i0
      do jdof = 1, ndof
        jj = j0 + jdof
        idxD = idxD + 1
        zz(jj) = hecMAT%D(idxD)
      end do
      ! Upper
      do j = hecMAT%indexU(i-1)+1,hecMAT%indexU(i)
        j0 = (hecMAT%itemU(j)-1)*ndof
        idxU0 = (j-1)*ndof2 + (idof-1)*ndof
        do jdof = 1, ndof
          jj = j0 + jdof
          idxU = idxU0 + jdof
          zz(jj) = hecMAT%AU(idxU)
        end do
      end do
      write(17,lineFormat) zz
    end do
  end do
  close(17)

end subroutine imf_dump_matrixLU


subroutine imf_load(filename,A)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  character*256 :: filename
  character*256 :: filename2
  character*256 :: type
  character(256) :: buf
  integer(kind=kint) :: R,C,NZ,i,NDOF,NDOF2
  type (imf_csr), pointer :: CSR
  type (imf_coo), pointer :: COO

  open(18,file=filename, status='old')
  READ(18,"(a)") filename2
  READ(18,"(a)") type
  READ(18,"(a)") buf
  if (trim(buf) .eq. "symmetric" .or. trim(buf) .eq. "1") then
    A%symmetric = .true.
  else
    A%symmetric = .false.
  end if 
  close(18)
  !filename = "../matrix/cap.bsr"
  write(*,*)"Start loading matrix"
  select case (type)
  case ('BSR')
    allocate(CSR)
    write(*,*)"BSR"
    call imf_load_csr(filename2,CSR)
    call imf_convert_csr2smat(CSR,A)
    deallocate(CSR)
  case ('CSR')
    allocate(CSR)
    write(*,*)"CSR"
    call imf_load_csr(filename2,CSR)
    call imf_convert_csr2smat(CSR,A)
    deallocate(CSR)
  case ('COO')
    allocate(COO)
    write(*,*)"COO"
    call imf_load_coo(filename2,COO)
    call imf_convert_coo2smat(COO,A)
  case default
    print *, "I don't know matrix type."
    stop
  end select

  !  call imf_unblock_csr(CSR)
  write(*,*)"Loaded matrix"


end subroutine imf_load
