module imf_util
  implicit none
  integer(kind=4), parameter :: kint=4
  integer(kind=4), parameter :: kreal=8

  type imf_smatrix
    integer(kind=kint) ::  N, NP, NPL, NPU, NDOF
    real(kind=kreal), pointer :: D(:)
    real(kind=kreal), pointer :: AL(:), AU(:)
    integer(kind=kint), pointer :: indexL(:), indexU(:)
    integer(kind=kint), pointer ::  itemL(:),  itemU(:)
    integer(kind=kint), pointer :: indexFL(:), indexFU(:)
    integer(kind=kint), pointer ::  itemFL(:),  itemFU(:)
    integer(kind=kint), pointer :: iperm(:)
    integer(kind=kint), pointer :: perm(:)
    logical :: symmetric = .true.
  end type imf_smatrix

  type imf_fmatrix
    integer(kind=kint) ::  N, NP, NDOF
    real(kind=kreal), pointer :: D(:)
    real(kind=kreal), pointer :: AL(:), AU(:)
    integer(kind=kint), pointer :: index(:)
    integer(kind=kint), pointer ::  item(:)
    logical :: symmetric = .true.
  end type imf_fmatrix

  type imf_csr
    integer(kind=kint) ::  N, NP,NDOF
    real(kind=kreal), pointer :: A(:)
    integer(kind=kint), pointer :: index(:)
    integer(kind=kint), pointer ::  item(:)
  end type imf_csr

  type imf_coo
    integer(kind=kint) ::  ROW, COL, NP, NDOF
    integer(kind=kint), pointer :: I(:)
    integer(kind=kint), pointer :: J(:)
    real(kind=kreal), pointer   :: A(:)
  end type imf_coo

  type imf_dcsr
    integer(kind=kint) ::  N, NP
    real(kind=kreal), pointer :: A(:)
    real(kind=kreal), pointer :: D(:)
    integer(kind=kint), pointer :: index(:)
    integer(kind=kint), pointer ::  item(:)
  end type imf_dcsr

  type imf_dmatrix
    integer(kind=kint) ::  N
    real(kind=kreal), pointer :: D(:)
    real(kind=kreal), pointer :: AL(:), AU(:)
    integer(kind=kint), pointer ::  itemL(:),  itemU(:)
  end type imf_dmatrix


  type imf_tree2
    integer(kind=kint ) :: n_descendant
    integer(kind=kint ), pointer :: descendant(:)
    integer(kind=kint ) :: n_child
    integer(kind=kint ), pointer :: child(:)
    integer(kind=kint ) :: n_ancestor
    integer(kind=kint ), pointer :: ancestor(:)
    integer(kind=kint ) :: snodes
    logical :: factorized
    logical :: updated
    real(kind=kreal),pointer :: update(:)
  end type imf_tree2

  type hecmw_fill
    integer(kind=kint) :: N = 0
    integer(kind=kint), pointer :: node(:) => null()
  endtype hecmw_fill
end module imf_util
