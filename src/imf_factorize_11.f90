recursive subroutine imf_factor_11(A,T,k,vL,vU)

  use imf_util
  implicit none
  type (imf_fmatrix) :: A
  type (imf_tree2) :: T(A%N)

  integer(kind=kint) :: i,j,k,l,in,jS,jE,N,ia,ib, NDOF, NDOF2,ii,ij 
  integer(kind=kint) :: n_decendant,n_ancestor,flag
  real(kind=kreal),pointer :: Update(:)
  integer(kind=kint),pointer :: perm(:), iperm(:)
  real(kind=kreal), pointer :: D(:)
  real(kind=kreal), pointer :: AL(:), AU(:)
  integer(kind=kint), pointer :: indexL(:), indexU(:)
  integer(kind=kint), pointer ::  itemL(:), itemU(:)
  integer(kind=kint) :: idxD,idxL,idxU,ivL,ivU, iv, idx
  real(kind=kreal) :: vL(1*A%N), vU(1*A%N)

  N     = A%N
  NDOF  = A%NDOF
  NDOF2 = A%NDOF*A%NDOF
  D     => A%D
  indexL=> A%index
  itemL => A%item
  AL    => A%AL
  indexU=> A%index
  itemU => A%item
  AU    => A%AU

  if (N>10) then 
    if (mod(k,N/10)==0) write(*,'(a,i0,a)') "Factrize [k=",k,"]"
  end if 
  n_decendant = T(k)%n_descendant        
  n_ancestor = T(k)%n_ancestor

  allocate(Update(NDOF2*n_ancestor*n_ancestor))
  !Update=>T(k)%update

  Update(1:NDOF2*n_ancestor*n_ancestor)=0.0d0

  allocate(iperm(1:N))

  !フロンタル行列番号と全体行列番号の置換列
  perm => T(k)%ancestor
  iperm(k) =  0
  do j = 1, n_ancestor
    in = T(k)%ancestor(j)
    iperm(in) = j
  end do

  !最適的呼び出しが必要なときは．v
  do j = 1,T(k)%n_child
    i = T(k)%child(j)
    if (T(i)%factorized .eqv. .false.) then
      call imf_factor_11(A,T,i,vL,vU)
    end if 
  end do

  idxD = NDOF2*(k-1)

  do i = 0, n_ancestor-1
    idx = NDOF2 * ( indexL(k-1) + i )
    iv = NDOF2 * (i+1)
    vL(iv+1) = AL(idx+1)
  end do

  do i = 0, n_ancestor-1
    idx = NDOF2 * ( indexU(k-1) + i )
    iv = NDOF2 * (i+1)
    vU(iv+1) = AU(idx+1)
  end do

  !Updateを足し込む．
  do j = 1, T(k)%n_child
    i = T(k)%child(j)
    D(idxD+1) = D(idxD+1) + T(i)%update(1)
    ia = iperm(T(i)%ancestor(1))
    do ij = 2, T(i)%n_ancestor
      ib = iperm(T(i)%ancestor(ij))
      ivL = ib*NDOF2
      idxU = (1-1)*NDOF2*T(i)%n_ancestor+(ij-1)*NDOF2
      idxL = (ij-1)*NDOF2*T(i)%n_ancestor+(1-1)*NDOF2
      vU(ivL+1) = vU(ivL+1) + T(i)%update(idxU+1)
      vL(ivL+1) = vL(ivL+1) + T(i)%update(idxL+1)
    end do 
    do ii = 2, T(i)%n_ancestor
      do ij = 2, T(i)%n_ancestor
        ia = iperm(T(i)%ancestor(ii))
        ib = iperm(T(i)%ancestor(ij))
        idxL = (ia-1)*NDOF2*n_ancestor+(ib-1)*NDOF2
        idxU = (ii-1)*NDOF2*T(i)%n_ancestor+(ij-1)*NDOF2
        Update(idxL+1) = Update(idxL+1) + T(i)%update(idxU+1)          
      end do
    end do     
    deallocate(T(i)%update)
  end do

  D(idxD+1) = 1.0d0 / D(idxD+1) 

  !(i,0)について (0はグローバルでkのこと．)
  do i = 0, n_ancestor-1
    idx = NDOF2 * ( indexL(k-1) + i )
    iv = NDOF2 * (i+1)
    AL(idx+1) = vL(iv+1)
  end do

  !(0,j)について
  do i = 0, n_ancestor-1
    idx = NDOF2 * ( indexU(k-1) + i )
    iv = NDOF2 * (i+1)
    AU(idx+1) = vU(iv+1)
  end do


  !(i,j)について
  do i = 1, n_ancestor
    do j = 1, n_ancestor
      idxU= (i-1)*n_ancestor*NDOF2+(j-1)*NDOF2
      ivL=i*NDOF2
      ivU=j*NDOF2
      Update(idxU+1) = Update(idxU+1)-vL(ivL+1)*vU(ivU+1)*D(idxD+1)
    end do
  end do 

  T(k)%update => Update
  T(k)%factorized = .true.
  deallocate(iperm)        

end subroutine imf_factor_11
