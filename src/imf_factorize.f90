subroutine imf_factorize(A,T)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  type (imf_fmatrix),allocatable :: F

  type (imf_tree2) :: T(A%N)
  integer(kind=kint) :: i,j,k,in,jS,jE,N,n_ancestor,NDOF,NDOF2
  integer(kind=kint) :: n_decendant
  real(kind=kreal),pointer :: Front(:,:,:), Update(:),Update2(:)
  real(kind=kreal),pointer :: vL(:),vU(:) 
  N=A%N
  NDOF=A%NDOF
  NDOF2=NDOF*NDOF
  allocate(vL(NDOF2*N))
  allocate(vU(NDOF2*N))
  allocate(F)
  call imf_convert_smat2fmat(A,F)
  !順序をどうするか．
  T(:)%snodes=1
  do i = 1, N-1
    !if (i<100) write(*,"(10(i0,2x))")i,T(i)%n_child,T(i)%child
    if(T(i)%n_child == 1 ) then
      k=i
      j=j+1
      do
        if (T(k)%n_ancestor==0) exit
        k=T(k)%ancestor(1)
        if(T(k)%n_child /= 1 ) exit
        T(i)%snodes=T(i)%snodes+1
      end do
      !if(k<100) write(*,*)i,k,T(i)%snodes
    end if 
  end do

  if (F%symmetric .eqv. .false.) then
    print '(a,i0,a,i0,a)', "Asymmetric ", NDOF, "x", NDOF, " Factorization"
    select case (NDOF)
    case (1)
      do i = 1, N
        call imf_factor_11(F,T,i,vL,vU)
      end do
    case (3)
      k=maxval(T(:)%n_ancestor)
      allocate(Update( NDOF2*k*k) )
      do i = 1, N
        call imf_factor_33(F,T,i,vL,vU)
      end do
    case default
      print '(a,i0,a)', "NOT available NDOF=", NDOF, " Asymmetric Factorization"
    end select
  else
    print '(a,i0,a,i0,a)', "Symmetric ", NDOF, "x", NDOF, " Factorization"
    select case (NDOF)
    case (1)
      do i = 1, N
        call imf_factor_11(F,T,i,vL,vU)
      end do
    case (3)
      do i = 1, N
        call imf_factor_33_sym(F,T,i,vL,vU)
      end do
    case default
      print '(a,i0,a)', "NOT available NDOF=", NDOF, " Symmetric Factorization"
    end select
    F%AL=F%AU
  end if 
  deallocate(vL)
  deallocate(vU)
  call imf_convert_fmat2smat(F,A)

end subroutine imf_factorize

