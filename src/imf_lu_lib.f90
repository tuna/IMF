!aa
module imf_lu_lib
  use imf_util

  private
  public:: imf_lu_factrization
  public:: imf_lu_get_result

  integer(kind=kint) :: N
  real(kind=kreal),   pointer :: D(:) => null()
  real(kind=kreal),   pointer :: AL(:) => null()
  real(kind=kreal),   pointer :: AU(:) => null()
  integer(kind=kint), pointer :: indexL(:) => null()
  integer(kind=kint), pointer :: indexU(:) => null()
  integer(kind=kint), pointer :: itemL(:) => null()
  integer(kind=kint), pointer :: itemU(:) => null()

contains
  subroutine imf_lu_factrization(MAT)
    implicit none
    integer(kind=kint ) :: i, j, k, ii, jj, kk, ij0
    integer(kind=kint ), allocatable :: iu(:), il(:)
    real   (kind=kreal) :: Dinv(3,3), RHS(3,3), Aik(3,3), Akj(3,3), MAT

    !D => MAT%D
    !AL=> MAT%AL
    !AU=> MAT%AU
    !indexL=> MAT%indexL
    !indexU=> MAT%indexU
    !itemL => MAT%itemL
    !itemU => MAT%itemU

    !**********  LU factorization  **********
    allocate (iu(N) , il(N))

    Dinv = 0.0d0
    RHS  = 0.0d0

    i = 1
    call Ddiag(Dinv, &
      D(9*i-8), D(9*i-7), D(9*i-6), &
      D(9*i-5), D(9*i-4), D(9*i-3), &
      D(9*i-2), D(9*i-1), D(9*i  ))

    D(9*i-8)= Dinv(1,1)
    D(9*i-7)= Dinv(1,2)
    D(9*i-6)= Dinv(1,3)
    D(9*i-5)= Dinv(2,1)
    D(9*i-4)= Dinv(2,2)
    D(9*i-3)= Dinv(2,3)
    D(9*i-2)= Dinv(3,1)
    D(9*i-1)= Dinv(3,2)
    D(9*i  )= Dinv(3,3)

    do i= 2, N
      iu= 0
      il= 0

      do k= indexL(i-1)+1, indexL(i)
        iu(itemL(k))= k
      enddo

      do k= indexU(i-1)+1, indexU(i)
        il(itemU(k))= k
      enddo

      do kk= itemL(i-1)+1, itemL(i)
        k= itemL(kk)

        Dinv(1,1)= D(9*k-8)
        Dinv(1,2)= D(9*k-7)
        Dinv(1,3)= D(9*k-6)
        Dinv(2,1)= D(9*k-5)
        Dinv(2,2)= D(9*k-4)
        Dinv(2,3)= D(9*k-3)
        Dinv(3,1)= D(9*k-2)
        Dinv(3,2)= D(9*k-1)
        Dinv(3,3)= D(9*k  )

        Aik(1,1)= AL(9*kk-8)
        Aik(1,2)= AL(9*kk-7)
        Aik(1,3)= AL(9*kk-6)
        Aik(2,1)= AL(9*kk-5)
        Aik(2,2)= AL(9*kk-4)
        Aik(2,3)= AL(9*kk-3)
        Aik(3,1)= AL(9*kk-2)
        Aik(3,2)= AL(9*kk-1)
        Aik(3,3)= AL(9*kk  )

        do jj= indexU(k-1)+1, indexU(k)
          j= itemU(jj)
          if (iu(j) == 0 .and. il(j) == 0) cycle

          Akj(1,1)= AU(9*jj-8)
          Akj(1,2)= AU(9*jj-7)
          Akj(1,3)= AU(9*jj-6)
          Akj(2,1)= AU(9*jj-5)
          Akj(2,2)= AU(9*jj-4)
          Akj(2,3)= AU(9*jj-3)
          Akj(3,1)= AU(9*jj-2)
          Akj(3,2)= AU(9*jj-1)
          Akj(3,3)= AU(9*jj  )

          call LUcolumn(RHS, Dinv, Aik, Akj)

          if (j.eq.i) then
            D(9*i-8)= D(9*i-8) - RHS(1,1)
            D(9*i-7)= D(9*i-7) - RHS(1,2)
            D(9*i-6)= D(9*i-6) - RHS(1,3)
            D(9*i-5)= D(9*i-5) - RHS(2,1)
            D(9*i-4)= D(9*i-4) - RHS(2,2)
            D(9*i-3)= D(9*i-3) - RHS(2,3)
            D(9*i-2)= D(9*i-2) - RHS(3,1)
            D(9*i-1)= D(9*i-1) - RHS(3,2)
            D(9*i  )= D(9*i  ) - RHS(3,3)
          endif

          if (j.lt.i) then
            ij0= iu(j)
            AL(9*ij0-8)= AL(9*ij0-8) - RHS(1,1)
            AL(9*ij0-7)= AL(9*ij0-7) - RHS(1,2)
            AL(9*ij0-6)= AL(9*ij0-6) - RHS(1,3)
            AL(9*ij0-5)= AL(9*ij0-5) - RHS(2,1)
            AL(9*ij0-4)= AL(9*ij0-4) - RHS(2,2)
            AL(9*ij0-3)= AL(9*ij0-3) - RHS(2,3)
            AL(9*ij0-2)= AL(9*ij0-2) - RHS(3,1)
            AL(9*ij0-1)= AL(9*ij0-1) - RHS(3,2)
            AL(9*ij0  )= AL(9*ij0  ) - RHS(3,3)
          endif

          if (j.gt.i) then
            ij0= il(j)
            AU(9*ij0-8)= AU(9*ij0-8) - RHS(1,1)
            AU(9*ij0-7)= AU(9*ij0-7) - RHS(1,2)
            AU(9*ij0-6)= AU(9*ij0-6) - RHS(1,3)
            AU(9*ij0-5)= AU(9*ij0-5) - RHS(2,1)
            AU(9*ij0-4)= AU(9*ij0-4) - RHS(2,2)
            AU(9*ij0-3)= AU(9*ij0-3) - RHS(2,3)
            AU(9*ij0-2)= AU(9*ij0-2) - RHS(3,1)
            AU(9*ij0-1)= AU(9*ij0-1) - RHS(3,2)
            AU(9*ij0  )= AU(9*ij0  ) - RHS(3,3)
          endif

        enddo
      enddo

      call Ddiag(Dinv, &
        D(9*i-8), D(9*i-7), D(9*i-6), &
        D(9*i-5), D(9*i-4), D(9*i-3), &
        D(9*i-2), D(9*i-1), D(9*i  ))

      D(9*i-8)= Dinv(1,1)
      D(9*i-7)= Dinv(1,2)
      D(9*i-6)= Dinv(1,3)
      D(9*i-5)= Dinv(2,1)
      D(9*i-4)= Dinv(2,2)
      D(9*i-3)= Dinv(2,3)
      D(9*i-2)= Dinv(3,1)
      D(9*i-1)= Dinv(3,2)
      D(9*i  )= Dinv(3,3)
    enddo

    deallocate (iu, il)
  end subroutine imf_lu_factrization

  subroutine Ddiag(ALU, D11,D12,D13,D21,D22,D23,D31,D32,D33)
    implicit none
    real(kind=kreal) :: ALU(3,3), PW(3)
    real(kind=kreal) :: D11,D12,D13,D21,D22,D23,D31,D32,D33
    integer(kind=kint) :: i,j,k

    ALU(1,1)= D11
    ALU(1,2)= D12
    ALU(1,3)= D13
    ALU(2,1)= D21
    ALU(2,2)= D22
    ALU(2,3)= D23
    ALU(3,1)= D31
    ALU(3,2)= D32
    ALU(3,3)= D33

    do k= 1, 3
      if (ALU(k,k) == 0.d0) then
        stop
      endif
      ALU(k,k)= 1.d0/ALU(k,k)
      do i= k+1, 3
        ALU(i,k)= ALU(i,k) * ALU(k,k)
        do j= k+1, 3
          PW(j)= ALU(i,j) - ALU(i,k)*ALU(k,j)
        enddo
        do j= k+1, 3
          ALU(i,j)= PW(j)
        enddo
      enddo
    enddo

  end subroutine Ddiag

  !C    computes L_ik * D_k_INV * U_kj at ILU factorization

  subroutine LUcolumn (RHS, Dinv, Aik, Akj)
    implicit none
    real(kind=kreal) :: RHS(3,3), Dinv(3,3), Aik(3,3), Akj(3,3)
    real(kind=kreal) :: X1,X2,X3

    !-- 1st Col.
    X1= Akj(1,1)
    X2= Akj(2,1)
    X3= Akj(3,1)

    X2= X2 - Dinv(2,1)*X1
    X3= X3 - Dinv(3,1)*X1 - Dinv(3,2)*X2

    X3= Dinv(3,3)*  X3
    X2= Dinv(2,2)*( X2 - Dinv(2,3)*X3 )
    X1= Dinv(1,1)*( X1 - Dinv(1,3)*X3 - Dinv(1,2)*X2)

    RHS(1,1)=  Aik(1,1)*X1 + Aik(1,2)*X2 + Aik(1,3)*X3
    RHS(2,1)=  Aik(2,1)*X1 + Aik(2,2)*X2 + Aik(2,3)*X3
    RHS(3,1)=  Aik(3,1)*X1 + Aik(3,2)*X2 + Aik(3,3)*X3

    !-- 2nd Col.
    X1= Akj(1,2)
    X2= Akj(2,2)
    X3= Akj(3,2)

    X2= X2 - Dinv(2,1)*X1
    X3= X3 - Dinv(3,1)*X1 - Dinv(3,2)*X2

    X3= Dinv(3,3)*  X3
    X2= Dinv(2,2)*( X2 - Dinv(2,3)*X3 )
    X1= Dinv(1,1)*( X1 - Dinv(1,3)*X3 - Dinv(1,2)*X2)

    RHS(1,2)=  Aik(1,1)*X1 + Aik(1,2)*X2 + Aik(1,3)*X3
    RHS(2,2)=  Aik(2,1)*X1 + Aik(2,2)*X2 + Aik(2,3)*X3
    RHS(3,2)=  Aik(3,1)*X1 + Aik(3,2)*X2 + Aik(3,3)*X3

    !-- 3rd Col.
    X1= Akj(1,3)
    X2= Akj(2,3)
    X3= Akj(3,3)

    X2= X2 - Dinv(2,1)*X1
    X3= X3 - Dinv(3,1)*X1 - Dinv(3,2)*X2

    X3= Dinv(3,3)*  X3
    X2= Dinv(2,2)*( X2 - Dinv(2,3)*X3 )
    X1= Dinv(1,1)*( X1 - Dinv(1,3)*X3 - Dinv(1,2)*X2)

    RHS(1,3)=  Aik(1,1)*X1 + Aik(1,2)*X2 + Aik(1,3)*X3
    RHS(2,3)=  Aik(2,1)*X1 + Aik(2,2)*X2 + Aik(2,3)*X3
    RHS(3,3)=  Aik(3,1)*X1 + Aik(3,2)*X2 + Aik(3,3)*X3

  end subroutine LUcolumn


  subroutine imf_lu_get_result(X)
    implicit none
    real(kind=kreal), intent(inout) :: X(:)
    integer(kind=kint) :: i, j, isL, ieL, isU, ieU, k
    real(kind=kreal) :: SW1, SW2, SW3, X1, X2, X3

    !-- FORWARD

    do i = 1, N
      SW1 = X(3*i-2)
      SW2 = X(3*i-1)
      SW3 = X(3*i  )
      isL = indexL(i-1)+1
      ieL = indexL(i)
      do j = isL, ieL
        k  = itemL(j)
        X1 = X(3*k-2)
        X2 = X(3*k-1)
        X3 = X(3*k  )
        SW1 = SW1 - AL(9*j-8)*X1-AL(9*j-7)*X2-AL(9*j-6)*X3
        SW2 = SW2 - AL(9*j-5)*X1-AL(9*j-4)*X2-AL(9*j-3)*X3
        SW3 = SW3 - AL(9*j-2)*X1-AL(9*j-1)*X2-AL(9*j  )*X3
      enddo

      X1 = SW1
      X2 = SW2
      X3 = SW3
      X2 = X2 - D(9*i-5)*X1
      X3 = X3 - D(9*i-2)*X1 - D(9*i-1)*X2
      X3 = D(9*i  )*  X3
      X2 = D(9*i-4)*( X2 - D(9*i-3)*X3 )
      X1 = D(9*i-8)*( X1 - D(9*i-6)*X3 - D(9*i-7)*X2)
      X(3*i-2) = X1
      X(3*i-1) = X2
      X(3*i  ) = X3
    enddo

    !C
    !C-- BACKWARD

    do i= N, 1, -1
      isU = indexU(i-1) + 1
      ieU = indexU(i)
      SW1 = 0.d0
      SW2 = 0.d0
      SW3 = 0.d0
      do j = ieU, isU, -1
        k  = itemU(j)
        X1 = X(3*k-2)
        X2 = X(3*k-1)
        X3 = X(3*k  )
        SW1 = SW1 + AU(9*j-8)*X1+AU(9*j-7)*X2+AU(9*j-6)*X3
        SW2 = SW2 + AU(9*j-5)*X1+AU(9*j-4)*X2+AU(9*j-3)*X3
        SW3 = SW3 + AU(9*j-2)*X1+AU(9*j-1)*X2+AU(9*j  )*X3
      enddo
      X1 = SW1
      X2 = SW2
      X3 = SW3
      X2 = X2 - D(9*i-5)*X1
      X3 = X3 - D(9*i-2)*X1 - D(9*i-1)*X2
      X3 = D(9*i  )*  X3
      X2 = D(9*i-4)*( X2 - D(9*i-3)*X3 )
      X1 = D(9*i-8)*( X1 - D(9*i-6)*X3 - D(9*i-7)*X2)
      X(3*i-2) = X(3*i-2) - X1
      X(3*i-1) = X(3*i-1) - X2
      X(3*i  ) = X(3*i  ) - X3
    enddo
  end subroutine imf_lu_get_result

  subroutine imf_lu_finalize()
    implicit none

    D  => null()
    AL => null()
    AU => null()
    indexL => null()
    indexU => null()
    itemL  => null()
    itemU  => null()

  end subroutine
end module imf_lu_lib
