
program imf_main

  use imf_util
  implicit none
  type (imf_smatrix), pointer :: A
  type (imf_smatrix), pointer :: B

  type (imf_tree2),pointer:: T(:)
  real(kind=kreal),pointer :: X(:), Y(:), Ans(:),XT(:),YT(:)
  real(kind=kreal) :: e

  character*256 :: filename

  integer i,j,k, N, NDOF
  integer t1, t2
  character(len=20) msg
  allocate(A)

  filename = "./imf_ctrl.dat"
  call imf_load(filename,A)
  N=A%N
  NDOF=A%NDOF
  allocate( X(A%N * A%NDOF) )
  allocate( Y(A%N * A%NDOF) )
  allocate( Ans(A%N * A%NDOF) )

  call imf_create_random_vector(A%N,A%NDOF,Ans)
  !AX=Y
  call imf_spmv(A,Ans,Y)

  call system_clock(t1)

  write(*,"(a,i0,a,i0,a,f8.5)")"N=",A%N," NZ=",A%NPL, " Density=",real(A%NPL)/real( (A%N-1)*A%N/2+1)
  write(*,"(a,f10.2,'MB')")" MATRIXMEM =", (2*kreal * A%NPL)/1024d0**2
  allocate(T(A%N))


  msg = "Reordering"
  call system_clock(t2)
  call imf_reordering(A)
  call imf_printtime(msg,t2)

  msg = "Permutation"
  call system_clock(t2)
  call imf_permutation(A)
  call imf_permutation_vector(A,Ans)
  call imf_permutation_vector(A,Y)       
  call imf_printtime(msg,t2)

  msg = "MakeTree"
  call system_clock(t2)
  call imf_maketree(A,T)
  call imf_printtime(msg,t2)

  msg = "Tree Allocation"
  call system_clock(t2)
  allocate(B)
  call imf_tree2allocate(A,B,T)
  call imf_printtime(msg,t2)

  write(*,"(a,i0,a,i0,a,f8.5)")"A: N=",A%N," NZ=",A%NPL, " Density=",real(A%NPL)/real( (A%N-1)*A%N/2+1)
  write(*,"(a,i0,a,i0,a,f8.5)")"B: N=",B%N," NZ=",B%NPL, " Density=",real(B%NPL)/real( (B%N-1)*B%N/2+1)
  write(*,"(a,f10.2,'MB')")" FACTORIZEMEM =", (2*kreal * B%NPL * B%NDOF * B%NDOF)/1024d0**2

  msg = "Copy matrix"
  call system_clock(t2)
  call imf_copymat(A,B)
  call imf_printtime(msg,t2)

  msg = "Factorize"
  call system_clock(t2)

  call imf_factorize(B,T)
  call imf_printtime(msg,t2)

  msg = "Substitutation"
  call system_clock(t2)
  X=Y
  call imf_substitution(B,X)
  call imf_printtime(msg,t2)

  e=0
  do i=1,A%N
    if (X(i)/=0.0) e=e+1.0
  end do 
  write(*,'(a,f15.0)') "L^0   =", e
  !write(*,'(a,e15.6)') "L^0.5 =",   (sum(abs(Ans-X) **0.5))**(2.0)
  write(*,'(a,e15.6)') "L^1   =",    sum(abs(Ans-X) )
  write(*,'(a,e15.6)') "L^2   =",   (sum(abs(Ans-X) **2))**(1.0/2.0)
  !write(*,'(a,e15.6)') "L^3   =",   (sum(abs(Ans-X) **3))**(1.0/3.0)
  write(*,'(a,e15.6)') "L^inf =", maxval(abs(Ans-X) )
  do i=1,A%N*A%NDOF
  end do

  msg = "Total "
  call imf_printtime(msg,t1)

end program imf_main
