
subroutine imf_maketree(A,T)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  type (imf_tree2) :: T(A%N)

  integer(kind=kint) :: i, j, k, l, jS, jE, in, c, pos
  integer(kind=kint) :: Nbytes, Start,End,range,parent
  integer(kind=kint), pointer :: Array(:)
  integer(kind=4) :: bit = 64
  integer(kind=8), pointer :: FillinMask(:)
  integer(kind=8), pointer :: ChildMask(:)
  integer(kind=8), pointer :: ParentMask(:)
  integer(kind=8) :: i8
  integer(kind=4) :: power = 6  !64は6ビットシフトでOK
  integer(kind=4) :: table(0:63)
  integer(kind=8) :: hash
  data hash/x'03F566ED27179461'/
  integer(kind=4) :: i4
  character(len=20) msg
  integer t1, t2

  real(kind=kint) :: float
  integer(kind=kint),allocatable :: diff(:)

  !NTZのためのハッシュ生成
  do i4 = 0, 63
    table( ishft(hash, -58) ) = i4
    hash = ishft(hash, 1)
  end do

  call system_clock(t2)
  msg =  "Making original tree"
  do i = 1, A%N
    T(i)%factorized = .false.
    T(i)%updated = .false.
    T(i)%n_descendant = 0
    T(i)%n_ancestor = 0
    jS= A%indexU(i-1) + 1
    jE= A%indexU(i  )   
    T(i)%n_ancestor = jE - jS + 1
    allocate(T(i)%ancestor(0:T(i)%n_ancestor))
    in=0
    T(i)%ancestor(in) = i
    do j = jS, jE
      in=in+1
      T(i)%ancestor(in) = A%itemU(j)
    end do
  end do
  call imf_printtime(msg,t2)

  call system_clock(t2)
  msg = "Determining Fill-in."

  Nbytes = ishft(A%N, -power)
  allocate( ChildMask( 0:Nbytes ) )
  allocate( ParentMask( 0:Nbytes ) )
  allocate( FillinMask( 0:Nbytes ) )

  !i=1から順にツリー作成・フィルイン判定を行う．
  do i = 1, A%N
    !親以外の成分がなければフィルインが起きようがないのでスキップ(0と1の場合)
    if (T(i)%n_ancestor<2) cycle
    !64で割る代わりに6ビット右シフトしている
    Start = ishft(i, -power)
    ! フィルインマスクをゼロクリア．
    ChildMask(Start:Nbytes) = 0
    ParentMask(Start:Nbytes) = 0

    parent = T(i)%ancestor(1)
    !1個目は直接の親なのでフィルイン判定に要らない．Cycleしてないのでj>=2が保証される．成分のある部分にビットを立てる．
    range = 0
    do j = 2, T(i)%n_ancestor
      in = T(i)%ancestor(j)
      End = ishft(in, -power)
      ChildMask(End) = ibset(ChildMask(End),mod(in,64))
      range = in
    end do
    k = T(parent)%n_ancestor
    !親側で成分のある部分にビットを立てる．
    do j = 1, k
      in = T(parent)%ancestor(j)
      End = ishft(in ,-power)
      !成分のあるところを1にしていく
      ParentMask(End) = ibset(ParentMask(End),mod(in,64))
      !親子で1番大きい成分位置をrangeに入れる
      range = max(range, in)
    end do
    !判定用ビットマスクの使う位置が決まる
    End = ishft(range, -power)

    !IORすると，親におけるフィルイン位置が定まる
    FillinMask(Start:End) = ior(  ChildMask(Start:End), ParentMask(Start:End)  )

    !フィルインをカウントする．
    c = 0
    do j = Start, End
      c = c + popcnt(FillinMask(j))
    end do

    if (c > 0) then
      allocate( Array(0:c) )
      Array(0) = parent
      T( parent )%n_ancestor = c
      in = 0
      do j = Start, End
        pos = ishft(j, power)
        do k = 1, popcnt(FillinMask(j))
          in = in + 1
          !Number of Training Zero (NTZ)を求めるテク．lで最下位ビットのみにしている．
          i8 = iand( FillinMask(j), -FillinMask(j) )
          ! l-1 したものをビットカウントして最下位ビットから何処か．
          c = popcnt(i8-1)
          ! M系列を使ったハッシュテーブルを用いて，最下位ビットを求める．本当は符号なし整数を使えたらiandがいらない．
          !AVXがOffのときはこちらのが早い．OnのときはわずかにPopcntが早い．
          !c = iand( table( ishft(285247320157033569_8*l, -58) ), 63 )
          !BSF命令が使えたらもっと良いはず．．
          !ビットクリアしてループ継続
          FillinMask(j) = iand( FillinMask(j), not(i8) )                
          !FillinMask(j) = ibclr(FillinMask(j),c) !こちらより上のほうが早いっぽい．
          !求めたNTZより成分番号を計算
          Array(in) = pos + c
        end do 
      end do
      !元のフィルイン決定したリストにポインタを付け替える．
      deallocate( T(parent)%ancestor)
      T(parent)%ancestor => Array
    end if 
    !5%おきにログ出し．
    !j=A%N/20
    !if (j==0) j=1
    !if ( mod(i,j)==0 ) write(*,"(i0,a,i0,i5,a)") i,"/",A%N, 100*i/A%N,"% OK "
  end do
  deallocate( ChildMask )
  deallocate( ParentMask )
  deallocate( FillinMask )
  call imf_printtime(msg,t2)

  call system_clock(t2)
  msg = "Determinant descendant list."
  !Descendantを作る．
  do i = 1,A%N
    do j = 1, T(i)%n_ancestor
      T( T(i)%ancestor(j) )%n_descendant = T( T(i)%ancestor(j) )%n_descendant + 1
    end do 
  end do
  do i = 1,A%N
    allocate(T(i)%descendant(T(i)%n_descendant))
  end do

  allocate( diff(A%N) )
  diff(:)=0
  do i = 1,A%N
    do j = 1, T(i)%n_ancestor
      diff( T(i)%ancestor(j) ) = diff( T(i)%ancestor(j) ) + 1
      T( T(i)%ancestor(j) )%descendant(diff( T(i)%ancestor(j) )) = i
    end do 
  end do
  call imf_printtime(msg,t2)


  call system_clock(t2)
  msg = "Determinant Child list."
  !Childを作る
  T(:)%n_child=0
  do i = 1,A%N
    if (T(i)%n_ancestor == 0 ) cycle
    parent = T(i)%ancestor(1)
    T( parent )%n_child = T( parent )%n_child  + 1
  end do
  do i = 1,A%N
    allocate(  T(i)%child(  T(i)%n_child  ))
  end do

  diff(:)=0
  do i = 1,A%N
    if (T(i)%n_ancestor == 0 ) cycle
    diff( T(i)%ancestor(1) ) = diff( T(i)%ancestor(1) ) + 1
    T( T(i)%ancestor(1) )%child(diff( T(i)%ancestor(1) )) = i
  end do
  call imf_printtime(msg,t2)
  deallocate( diff )
  !T(A%N)%ancestor(1)=-1

end subroutine imf_maketree




subroutine imf_maketreeNB(A,T)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  type (imf_tree2) :: T(A%N)

  integer(kind=kint) :: i,j,k,jS,jE,in,n,c
  integer(kind=kint) :: range,parent
  integer(kind=kint) :: ZERO = 0
  integer(kind=kint), pointer :: Array(:)
  integer(kind=kint), pointer :: Array2(:)
  integer(kind=kint) :: bit = kint*8
  integer(kind=kint),allocatable :: count(:), diff(:)

  Write(*,*) "Initialize Tree"

  Write(*,*) "Making original tree"
  do i = 1,A%N
    T(i)%factorized = .false.
    T(i)%updated = .false.
    T(i)%n_descendant = 0
    T(i)%n_ancestor = 0
    jS= A%indexU(i-1) + 1
    jE= A%indexU(i  )   
    T(i)%n_ancestor=jE - jS + 1
    allocate(T(i)%ancestor(T(i)%n_ancestor))
    n=0
    do j = jS, jE
      n=n+1
      T(i)%ancestor(n) = A%itemU(j)
    end do

  end do

  Write(*,*) "Determining Fill-in."



  !i=1から順にツリー作成・フィルイン判定を行う．
  do i = 1,A%N
    !親以外の成分がなければフィルインが起きようがないのでスキップ(0と1の場合)
    if (T(i)%n_ancestor<2) cycle

    parent = T(i)%ancestor(1)
    range = 0
    !最大数取る
    allocate(Array(T(i)%n_ancestor+T(parent)%n_ancestor-1))
    c=0  
    do j = 2, T(i)%n_ancestor
      c=c+1
      Array(c) = T(i)%ancestor(j)
      range=max(range,Array(c))
    end do
    k = T(parent)%n_ancestor
    !全部つなげる．
    do j = 1, k
      c=c+1
      Array(c) = T(parent)%ancestor(j)
      range=max(range,Array(c))
    end do
    in=1
    !とりあえずソート
    call quicksort_int_array(Array,in,c)
    do j=2,c
      if (Array(j-1)==Array(j))then
        !重複してたら最大値+1にする．
        Array(j-1)=range+1
      end if
    end do 
    !またソート
    call quicksort_int_array(Array,in,c)
    !最大値の位置を求める
    call bsearch_int_array(Array, in, c, range, n)

    T(parent)%n_ancestor=n
    allocate(Array2(n))
    Array2=Array(1:n)
    deallocate(Array)
    !元のフィルイン決定したリストにポインタを付け替える．
    deallocate( T(parent)%ancestor)
    T(parent)%ancestor => Array2
    !5%おきにログ出し．
    j=A%N/20
    if (j==0) j=1
    if ( mod(i,j)==0 ) write(*,"(i0,a,i0,i5,a)") i,"/",A%N, 100*i/A%N,"% OK "

  end do


  !Descendantを作る．
  do i = 1,A%N
    do j = 1, T(i)%n_ancestor
      T( T(i)%ancestor(j) )%n_descendant = T( T(i)%ancestor(j) )%n_descendant + 1
    end do 
  end do
  do i = 1,A%N
    allocate(T(i)%descendant(T(i)%n_descendant))
  end do

  allocate( diff(A%N) )
  diff(:)=0
  do i = 1,A%N
    do j = 1, T(i)%n_ancestor
      diff( T(i)%ancestor(j) ) = diff( T(i)%ancestor(j) ) + 1
      T( T(i)%ancestor(j) )%descendant(diff( T(i)%ancestor(j) )) = i
    end do 
  end do


end subroutine imf_maketreeNB

subroutine hecmw_matrix_get_fillin(hecMAT, idxU, itemU, NPU)
  use imf_util
  implicit none
  type(imf_smatrix) :: hecMAT
  type(hecmw_fill),   pointer:: F(:)
  integer(kind=kint), pointer :: idxU(:)
  integer(kind=kint), pointer :: itemU(:)
  integer(kind=kint) :: N, NPU
  integer(kind=kint) :: i, j, k, iS, iE, jS, jE
  integer(kind=kint) :: in, jn ,kn, nn, FN
  integer(kind=kint) :: imax, imin
  integer(kind=kint), pointer :: check(:), add(:), parent(:)

  N = hecMAT%N
  allocate(F(N))

  do i=1,N
    jS = hecMAT%indexU(i-1) + 1
    jE = hecMAT%indexU(i)
    in = jE - jS + 1
    F(i)%N = in
    allocate(F(i)%node(in))
    jn = 1
    do j=jS,jE
      in = hecMAT%itemU(j)
      F(i)%node(jn) = in
      jn = jn + 1
    enddo
  enddo

  !do i=1,N
  !  write(*,"(i5,a,i5,a,20i8)")i," N",F(i)%N,": ",F(i)%node(1:F(i)%N)
  !enddo
  !allocate(parent(N))
  !parent = 0
  do i=1,N
    !buget list
    in = F(i)%N
    imin = minval(F(i)%node)
    imax = maxval(F(i)%node)
    allocate(check(imin:imax))
    check(imin:imax) = 0
    do j=1,in
      check(F(i)%node(j)) = 1
    enddo
    nn = 0
    do j=imin,imax
      if(check(j)==1) nn = nn + 1
    enddo
    deallocate(F(i)%node)
    F(i)%N = nn
    allocate(F(i)%node(nn))
    in = 1
    do j=imin,imax
      if(check(j)==1)then
        F(i)%node(in) = j
        in = in + 1
      endif
    enddo
    deallocate(check)

    !add fill-in
    FN = F(i)%N
    if(1<FN)then
      in = F(i)%node(1)
      nn = FN - 1
      allocate(add(nn))
      add(1:nn) = F(i)%node(2:FN)
      call reallocate_array(F(in), nn, add)
      deallocate(add)
    endif
  enddo

  !do i=1,N
  !  write(*,"(i5,a,i5,a,20i8)")i," N",F(i)%N,": ",F(i)%node(1:F(i)%N)
  !enddo

  !count fill-in including diagonal entries
  allocate(idxU(0:N))
  in = 0
  idxU(0) = 0
  do i=1,N
    idxU(i) = idxU(i-1) + F(i)%N + 1
    in = in + F(i)%N + 1
  enddo
  NPU = in

  allocate(itemU(NPU))
  in = 0
  do i=1,N
    in = in + 1
    itemU(in) = i
    do j=1,F(i)%N
      in = in + 1
      itemU(in) = F(i)%node(j)
    enddo
  enddo
end subroutine hecmw_matrix_get_fillin
subroutine reallocate_array(F, num, x)
  use imf_util
  implicit none
  type(hecmw_fill) :: F
  integer(kind=kint) :: n, i, num, x(num)
  integer(kind=kint), pointer :: tmp(:)

  n = F%N
  tmp => F%node
  F%node => null()
  allocate(F%node(n+num))
  do i=1,n
    F%node(i) = tmp(i)
  enddo
  do i=1,num
    F%node(n+i) = x(i)
  enddo
  F%N = F%N + num
  deallocate(tmp)
end subroutine reallocate_array
