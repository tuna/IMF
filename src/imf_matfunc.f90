
subroutine imf_printtime(msg,t1)

  use imf_util
  implicit none
  character(len=20) msg
  integer t1, t2, t_rate, t_max, diff
  call system_clock(t2, t_rate, t_max)
  if ( t2 < t1 ) then
    diff = (t_max - t1) + t2 + 1
  else
    diff = t2 - t1
  endif
  print "(A,A, F10.3,A)", trim(msg), " time: ", diff/dble(t_rate), " s"

end subroutine



subroutine imf_spmv(A,X,Y)
  use imf_util
  type (imf_smatrix) :: A
  real(kind=kreal) :: X(A%N*A%NDOF)
  real(kind=kreal) :: Y(A%N*A%NDOF)
  integer(kind=kint) :: NDOF

  NDOF=A%NDOF
  if (NDOF == 1) then
    call imf_spmv_11(A,X,Y)
  else if (NDOF == 3) then
    call imf_spmv_33(A,X,Y)
  else if (NDOF == 6) then
    call imf_spmv_66(A,X,Y)
  end if
end subroutine

subroutine imf_spmv_11(A,X,Y)
  use imf_util
  type (imf_smatrix) :: A
  real(kind=kreal) :: X(A%N*A%NDOF)
  real(kind=kreal) :: Y(A%N*A%NDOF)
  integer(kind=kint) :: i, j, isL, ieL, isU, ieU, k,N,NDOF
  !-- FORWARD
  N=A%N
  NDOF=A%NDOF
  Y(:)=0.0d0
  do i = 1, N
    isL = A%indexL(i-1)+1
    ieL = A%indexL(i)
    do j = isL, ieL
      k  = A%itemL(j)
      Y(i) = Y(i) + A%AL(j)*X(k)
    enddo
  enddo
  do i = 1, N
    isU = A%indexU(i-1)+1
    ieU = A%indexU(i)
    do j = isU, ieU
      k  = A%itemU(j)
      Y(i) = Y(i) + A%AU(j)*X(k)
    enddo
  enddo
  do i = 1, N
    Y(i) = Y(i) + X(i)*A%D(i)
  enddo
end subroutine

subroutine imf_spmv_33(A,X,Y)
  use imf_util
  type (imf_smatrix) :: A
  real(kind=kreal) :: X(A%N*A%NDOF)
  real(kind=kreal) :: Y(A%N*A%NDOF)
  integer(kind=kint) :: i, j, jS, jE, N, NDOF
  real(kind=kreal) :: X1,X2,X3,Y1,Y2,Y3

  N=A%N
  NDOF=A%NDOF
  Y(:)=0.0d0

  do i = 1,N
    X1= X(3*i-2)
    X2= X(3*i-1)
    X3= X(3*i  )
    Y1= A%D(9*i-8)*X1 + A%D(9*i-7)*X2 + A%D(9*i-6)*X3
    Y2= A%D(9*i-5)*X1 + A%D(9*i-4)*X2 + A%D(9*i-3)*X3
    Y3= A%D(9*i-2)*X1 + A%D(9*i-1)*X2 + A%D(9*i  )*X3

    jS= A%indexL(i-1) + 1
    jE= A%indexL(i  )
    do j= jS, jE
      in  = A%itemL(j)
      X1= X(3*in-2)
      X2= X(3*in-1)
      X3= X(3*in  )
      Y2= Y2 + A%AL(9*j-5)*X1 + A%AL(9*j-4)*X2 + A%AL(9*j-3)*X3
      Y1= Y1 + A%AL(9*j-8)*X1 + A%AL(9*j-7)*X2 + A%AL(9*j-6)*X3
      Y3= Y3 + A%AL(9*j-2)*X1 + A%AL(9*j-1)*X2 + A%AL(9*j  )*X3
    enddo
    jS= A%indexU(i-1) + 1
    jE= A%indexU(i  )
    do j= jS, jE
      in  = A%itemU(j)
      X1= X(3*in-2)
      X2= X(3*in-1)
      X3= X(3*in  )
      Y1= Y1 + A%AU(9*j-8)*X1 + A%AU(9*j-7)*X2 + A%AU(9*j-6)*X3
      Y2= Y2 + A%AU(9*j-5)*X1 + A%AU(9*j-4)*X2 + A%AU(9*j-3)*X3
      Y3= Y3 + A%AU(9*j-2)*X1 + A%AU(9*j-1)*X2 + A%AU(9*j  )*X3
    enddo
    Y(3*i-2)= Y1
    Y(3*i-1)= Y2
    Y(3*i  )= Y3
  enddo

end subroutine

subroutine imf_spmv_66(A,X,Y)
  use imf_util
  type (imf_smatrix) :: A
  real(kind=kreal) :: X(A%N*A%NDOF)
  real(kind=kreal) :: Y(A%N*A%NDOF)
  integer(kind=kint) :: i, j, jS, jE, N, NDOF

  N=A%N
  NDOF=A%NDOF
  Y(:)=0.0d0
  do i = 1, N 
    X1= X(6*i-5)
    X2= X(6*i-4)
    X3= X(6*i-3)
    X4= X(6*i-2)
    X5= X(6*i-1)
    X6= X(6*i  )
    Y1= A%D(36*i-35)*X1 + A%D(36*i-34)*X2 + A%D(36*i-33)*X3 + A%D(36*i-32)*X4 + A%D(36*i-31)*X5 + A%D(36*i-30)*X6
    Y2= A%D(36*i-29)*X1 + A%D(36*i-28)*X2 + A%D(36*i-27)*X3 + A%D(36*i-26)*X4 + A%D(36*i-25)*X5 + A%D(36*i-24)*X6
    Y3= A%D(36*i-23)*X1 + A%D(36*i-22)*X2 + A%D(36*i-21)*X3 + A%D(36*i-20)*X4 + A%D(36*i-19)*X5 + A%D(36*i-18)*X6
    Y4= A%D(36*i-17)*X1 + A%D(36*i-16)*X2 + A%D(36*i-15)*X3 + A%D(36*i-14)*X4 + A%D(36*i-13)*X5 + A%D(36*i-12)*X6
    Y5= A%D(36*i-11)*X1 + A%D(36*i-10)*X2 + A%D(36*i-9 )*X3 + A%D(36*i-8 )*X4 + A%D(36*i-7 )*X5 + A%D(36*i-6 )*X6
    Y6= A%D(36*i-5 )*X1 + A%D(36*i-4 )*X2 + A%D(36*i-3 )*X3 + A%D(36*i-2 )*X4 + A%D(36*i-1 )*X5 + A%D(36*i   )*X6

    jS= A%indexL(i-1) + 1
    jE= A%indexL(i  )
    do j= jS, jE
      in  = A%itemL(j)
      X1= X(6*in-5)
      X2= X(6*in-4)
      X3= X(6*in-3)
      X4= X(6*in-2)
      X5= X(6*in-1)
      X6= X(6*in  )
      Y1= Y1 + A%AL(36*j-35)*X1 + A%AL(36*j-34)*X2 + A%AL(36*j-33)*X3 + A%AL(36*j-32)*X4 + A%AL(36*j-31)*X5 + A%AL(36*j-30)*X6
      Y2= Y2 + A%AL(36*j-29)*X1 + A%AL(36*j-28)*X2 + A%AL(36*j-27)*X3 + A%AL(36*j-26)*X4 + A%AL(36*j-25)*X5 + A%AL(36*j-24)*X6
      Y3= Y3 + A%AL(36*j-23)*X1 + A%AL(36*j-22)*X2 + A%AL(36*j-21)*X3 + A%AL(36*j-20)*X4 + A%AL(36*j-19)*X5 + A%AL(36*j-18)*X6
      Y4= Y4 + A%AL(36*j-17)*X1 + A%AL(36*j-16)*X2 + A%AL(36*j-15)*X3 + A%AL(36*j-14)*X4 + A%AL(36*j-13)*X5 + A%AL(36*j-12)*X6
      Y5= Y5 + A%AL(36*j-11)*X1 + A%AL(36*j-10)*X2 + A%AL(36*j-9 )*X3 + A%AL(36*j-8 )*X4 + A%AL(36*j-7 )*X5 + A%AL(36*j-6 )*X6
      Y6= Y6 + A%AL(36*j-5 )*X1 + A%AL(36*j-4 )*X2 + A%AL(36*j-3 )*X3 + A%AL(36*j-2 )*X4 + A%AL(36*j-1 )*X5 + A%AL(36*j   )*X6
    enddo
    jS= A%indexU(i-1) + 1
    jE= A%indexU(i  )
    do j= jS, jE
      in  = A%itemU(j)
      X1= X(6*in-5)
      X2= X(6*in-4)
      X3= X(6*in-3)
      X4= X(6*in-2)
      X5= X(6*in-1)
      X6= X(6*in  )
      Y1= Y1 + A%AU(36*j-35)*X1 + A%AU(36*j-34)*X2 + A%AU(36*j-33)*X3 + A%AU(36*j-32)*X4 + A%AU(36*j-31)*X5 + A%AU(36*j-30)*X6
      Y2= Y2 + A%AU(36*j-29)*X1 + A%AU(36*j-28)*X2 + A%AU(36*j-27)*X3 + A%AU(36*j-26)*X4 + A%AU(36*j-25)*X5 + A%AU(36*j-24)*X6
      Y3= Y3 + A%AU(36*j-23)*X1 + A%AU(36*j-22)*X2 + A%AU(36*j-21)*X3 + A%AU(36*j-20)*X4 + A%AU(36*j-19)*X5 + A%AU(36*j-18)*X6
      Y4= Y4 + A%AU(36*j-17)*X1 + A%AU(36*j-16)*X2 + A%AU(36*j-15)*X3 + A%AU(36*j-14)*X4 + A%AU(36*j-13)*X5 + A%AU(36*j-12)*X6
      Y5= Y5 + A%AU(36*j-11)*X1 + A%AU(36*j-10)*X2 + A%AU(36*j-9 )*X3 + A%AU(36*j-8 )*X4 + A%AU(36*j-7 )*X5 + A%AU(36*j-6 )*X6
      Y6= Y6 + A%AU(36*j-5 )*X1 + A%AU(36*j-4 )*X2 + A%AU(36*j-3 )*X3 + A%AU(36*j-2 )*X4 + A%AU(36*j-1 )*X5 + A%AU(36*j   )*X6
    enddo
    Y(6*i-5)= Y1
    Y(6*i-4)= Y2
    Y(6*i-3)= Y3
    Y(6*i-2)= Y4
    Y(6*i-1)= Y5
    Y(6*i  )= Y6
  enddo

end subroutine





subroutine imf_tree2allocate(A,B,T)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  type (imf_smatrix) :: B
  type (imf_tree2) :: T(A%N)
  integer(kind=kint) :: i,j,k,in,jS,jE,N,NZ,NDOF,NDOF2

  N=A%N
  NDOF=A%NDOF
  NDOF2=NDOF*NDOF
  NZ=0
  do i = 1, N
    NZ = NZ + T(i)%n_ancestor
  end do
  call imf_init_matrix(B,N,NDOF,NZ)
  B%indexL(0)=0
  do i = 1,N
    B%indexL(i) = B%indexL(i-1) + T(i)%n_descendant
    do j = 1,T(i)%n_descendant
      B%itemL( B%indexL(i-1)+j ) = T(i)%descendant(j)
    end do
  end do  

  B%indexU(0)=0
  do i = 1,N
    B%indexU(i) = B%indexU(i-1) + T(i)%n_ancestor
    do j = 1,T(i)%n_ancestor
      B%itemU( B%indexU(i-1)+j ) = T(i)%ancestor(j)
    end do
  end do  

  allocate(B%AL(NZ*NDOF2))
  B%AL(:)=0.0d0
  allocate(B%AU(NZ*NDOF2))
  B%AU(:)=0.0d0
  allocate(B%D(N*NDOF2))

  !call imf_transposeU(B)

end subroutine imf_tree2allocate

subroutine imf_copymat(A,B)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  type (imf_smatrix) :: B
  integer(kind=kint) :: i,j,k,l,inA,inB,jS,jE,N,NZ,AS,AE,BS,BE,NDOF2,NDOF

  N=A%N
  NDOF=A%NDOF
  NDOF2=NDOF*NDOF

  B%D(1:N*NDOF2) = A%D(1:N*NDOF2)
  B%AL(:)=0.0d0
  B%AU(:)=0.0d0
  B%symmetric=A%symmetric

  do i = 1,N
    AS= A%indexL(i-1) + 1
    AE= A%indexL(i  )          
    BS= B%indexL(i-1) + 1
    BE= B%indexL(i  )          
    do j = AS, AE
      inA = A%itemL(j)
      do k = BS, BE
        inB = B%itemL(k)
        if (inA == inB) then
          do l = 1,NDOF2
            B%AL(NDOF2*k-NDOF2+l) = A%AL(NDOF2*j-NDOF2+l)
          end do
          BS = k+1
          exit                
        end if
      end do
    end do
  end do
  do i = 1,N
    AS= A%indexU(i-1) + 1
    AE= A%indexU(i  )          
    BS= B%indexU(i-1) + 1
    BE= B%indexU(i  )          
    do j = AS, AE
      inA = A%itemU(j)
      do k = BS, BE
        inB = B%itemU(k)
        if (inA == inB) then
          do l = 1,NDOF2
            B%AU(NDOF2*k-NDOF2+l) = A%AU(NDOF2*j-NDOF2+l)
          end do
          BS = k+1
          exit                
        end if
      end do
    end do
  end do

end subroutine imf_copymat

subroutine imf_create_random_vector(N,NDOF,X)
  use imf_util
  integer(kind=kint) :: i,N,NDOF
  real (kind=kreal) :: X(N*NDOF)
  !-1 から 1までのrandomを生成。
  do i = 1, N*NDOF
    !call random_number(X(i))
    X(i)=(i)*2.0
  end do 
end subroutine



subroutine imf_transposeL2U(A)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  integer(kind=kint) :: i,j,in,jS,jE,N
  integer(kind=kint),Allocatable :: count(:),diff(:)

  N = A%N
  allocate( count(N) )
  allocate( diff(N) )
  count(:)=0
  diff(:)=0
  A%NPU = A%NPL

  do i = 1,A%N
    !zz(i)=A%D(i)
    jS= A%indexL(i-1) + 1
    jE= A%indexL(i  )
    do j= jS, jE
      in = A%itemL(j)
      count(in)=count(in)+1
    end do
  end do

  A%indexU(0)=0
  do i = 1,A%N
    A%indexU(i)=A%indexU(i-1)+count(i)
  end do

  do i = 1,N
    jS= A%indexL(i-1) + 1
    jE= A%indexL(i  )
    do j= jS, jE
      in = A%itemL(j)
      A%AU( A%indexU(in-1)+1+diff(in) ) = A%AL(j)
      A%itemU( A%indexU(in-1)+1+diff(in) ) = i
      diff(in) = diff(in) + 1
    end do
  end do

  deallocate(count)

end subroutine imf_transposeL2U

subroutine imf_transposeL(A)

  use imf_util
  implicit none
  type (imf_smatrix) :: A

  call imf_transpose(A%N,A%NDOF,A%NPL,A%indexL,A%itemL,A%AL,A%NPU,A%indexU,A%itemU,A%AU)

end subroutine imf_transposeL

subroutine imf_transposeU(A)

  use imf_util
  implicit none
  type (imf_smatrix) :: A

  call imf_transpose(A%N,A%NDOF,A%NPU,A%indexU,A%itemU,A%AU,A%NPL,A%indexL,A%itemL,A%AL)

end subroutine imf_transposeU

subroutine imf_transpose(N, NDOF, NPL, indexL, itemL, AL, NPU, indexU, itemU, AU)

  use imf_util
  implicit none
  integer(kind=kint) :: i,j,k,in,jS,jE,l
  integer(kind=kint) :: N,NPL,NPU,NDOF,NDOF2
  integer(kind=kint) :: indexL(0:N), itemL(NPL)
  integer(kind=kint) :: indexU(0:N), itemU(NPU)
  real(kind=kreal) :: AL(NPL*NDOF*NDOF)
  real(kind=kreal) :: AU(NPU*NDOF*NDOF)

  integer(kind=kint),allocatable :: count(:), diff(:)

  allocate( count(N) )
  allocate( diff(N) )
  count(:)=0
  diff(:)=0
  NDOF2=NDOF*NDOF
  do i = 1,N
    jS= indexL(i-1) + 1
    jE= indexL(i  )
    do j= jS, jE
      in = itemL(j)
      count(in)=count(in)+1
    end do
  end do

  indexU(0)=0
  do i = 1,N
    indexU(i)=indexU(i-1)+count(i)
  end do

  do i = 1,N
    jS= indexL(i-1) + 1
    jE= indexL(i  )
    do j= jS, jE
      in = itemL(j)
      !AU( indexU(in-1)+1+diff(in) ) = AL(j)
      do k = 1, NDOF
        do l = 1, NDOF
          AU( NDOF2*( indexU(in-1)+1+diff(in) ) -NDOF2+ (k-1)*NDOF+l ) = AL( NDOF2*(j-1) + (l-1)*NDOF+k )
        end do 
      end do

      itemU( indexU(in-1)+1+diff(in) ) = i
      diff(in) = diff(in) + 1
    end do
  end do

  deallocate(diff)
  deallocate(count)

end subroutine imf_transpose


subroutine imf_transpose_profile(N, NDOF, NPL, indexL, itemL, AL, NPU, indexU, itemU, AU)

  use imf_util
  implicit none
  integer(kind=kint) :: i,j,k,in,jS,jE,l
  integer(kind=kint) :: N,NPL,NPU,NDOF,NDOF2
  integer(kind=kint) :: indexL(0:N), itemL(NPL)
  integer(kind=kint) :: indexU(0:N), itemU(NPU)
  real(kind=kreal) :: AL(NPL*NDOF*NDOF)
  real(kind=kreal) :: AU(NPU*NDOF*NDOF)

  integer(kind=kint),allocatable :: count(:), diff(:)

  allocate( count(N) )
  allocate( diff(N) )
  count(:)=0
  diff(:)=0
  NDOF2=NDOF*NDOF
  do i = 1,N
    jS= indexL(i-1) + 1
    jE= indexL(i  )
    do j= jS, jE
      in = itemL(j)
      count(in)=count(in)+1
    end do
  end do

  indexU(0)=0
  do i = 1,N
    indexU(i)=indexU(i-1)+count(i)
  end do

  do i = 1,N
    jS= indexL(i-1) + 1
    jE= indexL(i  )
    do j= jS, jE
      in = itemL(j)
      !AU( indexU(in-1)+1+diff(in) ) = AL(j)
      do k = 1, NDOF
        do l = 1, NDOF
          AU( NDOF2*( indexU(in-1)+1+diff(in) ) -NDOF2+ (k-1)*NDOF+l ) = AL( NDOF2*(j-1) + (k-1)*NDOF+l )
        end do 
      end do

      itemU( indexU(in-1)+1+diff(in) ) = i
      diff(in) = diff(in) + 1
    end do
  end do

  deallocate(diff)
  deallocate(count)

end subroutine imf_transpose_profile


subroutine imf_init_matrix(mat,N,NDOF,NZ)

  use imf_util
  implicit none
  type (imf_smatrix) :: mat
  integer(kind=kint) :: N,NZ,NDOF,NDOF2
  NDOF2=NDOF*NDOF
  allocate(mat%D(N*NDOF2))
  allocate(mat%AL(NZ*NDOF2))
  allocate(mat%AU(NZ*NDOF2))

  allocate(mat%indexL(0:N))
  allocate(mat%itemL(NZ))
  allocate(mat%indexU(0:N))
  allocate(mat%itemU(NZ))

  mat%N=N
  mat%NDOF=NDOF
  mat%NPL = NZ
  mat%NPU = NZ

  mat%D(:)=0.0d0
  mat%AL(:)=0.0d0
  mat%AU(:)=0.0d0
  mat%indexL(:)=0
  mat%indexU(:)=0
  mat%itemL(:)=0
  mat%itemU(:)=0

end subroutine imf_init_matrix


subroutine debug(mat)

  use imf_util
  implicit none
  type (imf_smatrix) :: mat
  write(*,*) "D"
  write(*,*) mat%D
  write(*,*) "AL"
  write(*,*) mat%AL
  write(*,*) "INDEXL"
  write(*,*) mat%indexL
  write(*,*) "ITEML"
  write(*,*) mat%itemL
  write(*,*) "INDEXU"
  write(*,*) mat%indexU
  write(*,*) "ITEMU"
  write(*,*) mat%itemU


end subroutine debug


recursive subroutine quicksort_int_array_perm(a, perm, first, last)
  use imf_util
  implicit none
  integer(kind=kint)::   a(*)
  integer(kind=kint):: perm(*)
  integer(kind=kint):: first, last
  integer(kind=kint):: i, j
  integer(kind=kint)::  x, t

  if (first >= last) then
    RETURN
  end if

  x = a( (first+last) / 2 )
  i = first
  j = last
  do
    do while (a(i) < x)
      i=i+1
    end do
    do while (x < a(j))
      j=j-1
    end do
    if (i >= j) exit
    t = a(i);  a(i) = a(j);  a(j) = t
    t = perm(i);  perm(i) = perm(j);  perm(j) = t
    i=i+1
    j=j-1
  end do
  if (first < i - 1) call quicksort_int_array_perm(a,perm, first, i - 1 )
  if (j + 1 < last)  call quicksort_int_array_perm(a,perm,  j + 1, last )
end subroutine quicksort_int_array_perm


recursive subroutine quicksort_int_array(a, first, last)
  use imf_util
  implicit none
  integer(kind=kint)::  a(*)
  integer(kind=kint):: first, last
  integer(kind=kint):: i, j
  integer(kind=kint)::  x, t


  if (first >= last) then
    RETURN
  end if

  x = a( (first+last) / 2 )
  i = first
  j = last
  do
    do while (a(i) < x)
      i=i+1
    end do
    do while (x < a(j))
      j=j-1
    end do
    if (i >= j) exit
    t = a(i);  a(i) = a(j);  a(j) = t
    i=i+1
    j=j-1
  end do
  if (first < i - 1) call quicksort_int_array(a, first, i - 1)
  if (j + 1 < last)  call quicksort_int_array(a, j + 1, last)
end subroutine quicksort_int_array


subroutine bsearch_int_array(array, istart, iend, val, idx)
  use imf_util
  implicit none
  integer(kind=kint), intent(in) :: array(*)
  integer(kind=kint), intent(in) :: istart, iend
  integer(kind=kint), intent(in) :: val
  integer(kind=kint), intent(out) :: idx
  integer(kind=kint) :: center, left, right, pivot
  left = istart
  right = iend
  do
    if (left > right) then
      idx = -1
      exit
    end if
    center = (left + right) / 2
    pivot = array(center)
    if (val < pivot) then
      right = center - 1
      cycle
    else if (pivot < val) then
      left = center + 1
      cycle
    else ! if (pivot == val) then
      idx = center
      exit
    end if
  end do
end subroutine bsearch_int_array

subroutine imf_convert_smat2dcsr(SMAT,CSR)

  use imf_util
  implicit none
  type (imf_smatrix) :: SMAT
  type (imf_DCSR) :: CSR
  integer(kind=kint) :: i,j,in,jS,jE,N
  integer(kind=kint) :: NP,NPU,CL,CU,C

  N = SMAT%N
  NP=0
  do i = 1,SMAT%N
    NP = NP + SMAT%indexL(i  )-SMAT%indexL(i-1)
    NP = NP + SMAT%indexU(i  )-SMAT%indexU(i-1)
  end do
  CSR%NP=NP
  allocate(CSR%A(NP))
  allocate(CSR%D(N))
  allocate(CSR%index(0:N))
  allocate(CSR%item(NP))
  CSR%N = N
  CSR%A(:)=0d0

  CSR%index(0)=0
  C=0
  do i = 1, N
    CSR%D(i) = SMAT%D(i)
    jS= SMAT%indexL(i-1) + 1
    jE= SMAT%indexL(i  )
    do j= jS, jE
      C=C+1
      in = SMAT%itemL(j)
      CSR%item(C) = in
      CSR%A(C) = SMAT%AL(j)
    end do
    jS= SMAT%indexU(i-1) + 1
    jE= SMAT%indexU(i  )
    do j= jS, jE
      C=C+1
      in = SMAT%itemU(j)
      CSR%item(C) = in
      CSR%A(C) = SMAT%AU(j)
    end do
    CSR%index(i) = C
  end do
end subroutine imf_convert_smat2dcsr

subroutine imf_convert_csr2smat(CSR,SMAT)

  use imf_util
  implicit none
  type (imf_csr) :: CSR
  type (imf_smatrix) :: SMAT
  integer(kind=kint) :: i,j,in,jS,jE,N,k
  integer(kind=kint) :: NPL,NPU,CL,CU,NDOF,NDOF2
  NDOF = CSR%NDOF
  NDOF2 = NDOF*NDOF
  N = CSR%N
  SMAT%NP=CSR%NP
  NPL=0
  NPU=0
  do i = 1,CSR%N
    jS= CSR%index(i-1) + 1
    jE= CSR%index(i  )
    do j= jS, jE
      in = CSR%item(j)
      if(i < in) then
        NPL=NPL+1
      else if (i > in) then
        NPU=NPU+1
      else if(i == in) then
      end if 
    end do
  end do
  SMAT%N = N
  SMAT%NDOF=NDOF
  SMAT%NPL=NPL
  SMAT%NPU=NPU
  allocate(SMAT%D(NDOF2*N))

  allocate(SMAT%AL(NDOF2*NPL))
  allocate(SMAT%AU(NDOF2*NPU))
  allocate(SMAT%indexL(0:N))
  allocate(SMAT%itemL(NPL))
  allocate(SMAT%indexU(0:N))
  allocate(SMAT%itemU(NPU))
  SMAT%D(:)=0d0
  SMAT%AL(:)=0d0
  SMAT%AU(:)=0d0
  CL=0
  CU=0
  SMAT%indexL(0)=0
  SMAT%indexU(0)=0

  do i = 1,CSR%N
    jS= CSR%index(i-1) + 1
    jE= CSR%index(i  )
    do j= jS, jE
      in = CSR%item(j)
      if(i < in) then
        CL=CL+1
        SMAT%itemL(CL) = in
        do k = 1,NDOF2
          SMAT%AL(NDOF2*CL -NDOF2+k) = CSR%A(NDOF2*j -NDOF2+k)
        end do
      else if (i > in) then
        CU=CU+1
        SMAT%itemU(CU) = in
        do k = 1,NDOF2
          SMAT%AU(NDOF2*CU -NDOF2+k) = CSR%A(NDOF2*j -NDOF2+k)
        end do
      else if(i == in) then
        do k = 1,NDOF2
          SMAT%D(NDOF2*i -NDOF2+k) = CSR%A(NDOF2*j -NDOF2+k)
        end do
      end if 
    end do
    SMAT%indexL(i) = CL
    SMAT%indexU(i) = CU
  end do

end subroutine imf_convert_csr2smat

subroutine imf_convert_coo2smat(COO,SMAT)

  use imf_util
  implicit none
  type (imf_coo) :: COO
  type (imf_smatrix) :: SMAT
  integer(kind=kint) :: i,j,in,jS,jE,N,k,l,NZ
  integer(kind=kint),pointer :: vi(:),vj(:),it(:),index(:),perm(:),item(:),indexL(:),indexU(:),itemL(:),itemU(:)
  real(kind=kreal),pointer   :: rt(:),A(:),AL(:),AU(:),D(:)
  integer(kind=kint) :: NPL,NPU,CL,CU,NDOF,NDOF2
  NDOF = COO%NDOF
  NDOF2 = NDOF*NDOF
  N = COO%ROW
  NZ = COO%NP
  NPL=0
  NPU=0
  do k = 1, NZ
    if ( COO%I(k) < COO%J(k) ) then
      NPL=NPL+1
    else if ( COO%I(k) > COO%J(k) ) then
      NPU=NPU+1
    end if
  end do
  vi=>COO%I
  item=>COO%J
  A=>COO%A
  !do k = 1, NZ
  !  write(*,"(a,i0,a,i0,a,e13.5)")"(",vi(k),",",item(k),")=",A(k)
  !end do
  allocate(rt(NZ))
  allocate(it(NZ))
  allocate(perm(NZ))
  allocate(index(0:N))
  do k=1,NZ
    perm(k)=k
  end do
  l=1
  call quicksort_int_array_perm(vi,perm,l,NZ)
  index(:)=0
  do k = 1,NZ
    index(vi(k))=k
    it(k) = item(perm(k))
  end do
  deallocate(item)
  item=>it
  do k=1,N
    call quicksort_int_array_perm(item,perm,index(k-1)+1,index(k))
  end do
  do k = 1,NZ
    rt(k) = A(perm(k))
  end do
  deallocate(A)
  A=>rt
  !do k = 1, NZ
  !  write(*,"(a,i0,a,i0,a,e13.5)")"(",vi(k),",",item(k),")=",A(k)
  !end do
  deallocate(perm)
  allocate(D(NDOF2*N))
  allocate(AL(NDOF2*NPL))
  allocate(AU(NDOF2*NPU))
  allocate(indexL(0:N))
  allocate(itemL(NPL))
  allocate(indexU(0:N))
  allocate(itemU(NPU))
  SMAT%N = N
  SMAT%NP=COO%NP
  SMAT%NDOF=NDOF
  SMAT%NPL=NPL
  SMAT%NPU=NPU
  SMAT%D=>D
  SMAT%AL=>AL
  SMAT%AU=>AU
  SMAT%indexL=>indexL
  SMAT%itemL=>itemL
  SMAT%indexU=>indexU
  SMAT%itemU=>itemU

  D(:)=0d0
  AL(:)=0d0
  AU(:)=0d0
  CL=0
  CU=0
  indexL(0)=0
  indexU(0)=0

  do i = 1,N
    jS= index(i-1) + 1
    jE= index(i  )
    do j= jS, jE
      in = item(j)
      if(i < in) then
        CL=CL+1
        itemL(CL) = in
        do k = 1,NDOF2
          AL(NDOF2*CL -NDOF2+k) = A(NDOF2*j -NDOF2+k)
        end do
      else if (i > in) then
        CU=CU+1
        itemU(CU) = in
        do k = 1,NDOF2
          AU(NDOF2*CU -NDOF2+k) = A(NDOF2*j -NDOF2+k)
        end do
      else if(i == in) then
        do k = 1,NDOF2
          D(NDOF2*i -NDOF2+k) = A(NDOF2*j -NDOF2+k)
        end do
      end if
    end do
    indexL(i) = CL
    indexU(i) = CU
  end do
  deallocate(index)

end subroutine imf_convert_coo2smat

subroutine imf_convert_csr_block(CSR,factor)

  use imf_util
  implicit none
  type (imf_csr) :: CSR
  type (imf_smatrix) :: SMAT
  integer(kind=kint) :: i,j,in,jS,jE,N,k,aa,bb,cc,BNDOF,BNDOF2,newndof
  integer(kind=kint) :: NP,NDOF,NDOF2
  real(kind=kreal), pointer :: A(:)
  real(kind=kreal), pointer :: B(:)
  integer(kind=kint),pointer :: index(:)
  integer(kind=kint),pointer :: item(:)
  integer(4) :: factor

  N = CSR%N
  NP = CSR%NP
  NDOF = CSR%NDOF
  NDOF2 = NDOF * NDOF
  BNDOF = NDOF * factor
  BNDOF2 = BNDOF * BNDOF
  index => CSR%index
  item => CSR%item
  A => CSR%A

  allocate(B( 1 : NP*factor*factor ))

  do i = 1,N
    jS= index(i-1) + 1
    jE= index(i  )
    do j= jS, jE
      in = item(j)
      do aa = 1, BNDOF
        do bb= 1, BNDOF
          if (aa>NDOF) then 
            if (aa == bb) then                     
              B(BNDOF2*j -BNDOF2 + (aa-1)*BNDOF+bb) = 1
            else
              B(BNDOF2*j -BNDOF2 + (aa-1)*BNDOF+bb) = 0
            end if  
          else
            if (bb>NDOF) then
              B(BNDOF2*j -BNDOF2 + (aa-1)*BNDOF+bb) = 0
            else
              B(BNDOF2*j -BNDOF2 + (aa-1)*BNDOF+bb) = A(NDOF2*j -NDOF2 + (aa-1)*NDOF+bb)
            end if  
          end if                  
        end do 
      end do 
    end do
  end do
  CSR%NDOF=BNDOF
  CSR%A=>B
  deallocate(A)

end subroutine imf_convert_csr_block      

subroutine imf_unblock_csr(CSR)

  use imf_util
  implicit none
  type (imf_csr) :: CSR
  type (imf_smatrix) :: SMAT
  integer(kind=kint) :: i,j,in,jS,jE,N,k,ii,ij,l,pos,ni
  integer(kind=kint) :: NP,NDOF,NDOF2
  real(kind=kreal), pointer :: A(:)
  integer(kind=kint),pointer :: index(:)
  integer(kind=kint),pointer :: item(:)

  N     = CSR%N
  NP    = CSR%NP
  NDOF  = CSR%NDOF
  NDOF2 = NDOF * NDOF

  allocate(index(0:N*NDOF))
  allocate(item(1:NP*NDOF2))
  allocate(A(1:NP*NDOF2))
  index(0) = 0
  do i = 1, N
    jS = CSR%index(i-1) + 1
    jE = CSR%index(i  )
    do ii = 1, NDOF
      index((i-1)*NDOF+ii) = CSR%index(i-1)*NDOF2 + (ii)*NDOF*(jE-jS+1)
    end do 
    do j = jS, jE
      in = CSR%item(j)
      do ii = 1, NDOF
        ni = index((i-1)*NDOF) + (ii-1)*(jE-jS+1)*NDOF
        do ij = 1, NDOF
          pos = (j-1)*NDOF2+1 + (ii-1)*NDOF + (ij-1)
          l = ni  + (j-jS)*NDOF + ij 
          item(l) = (in-1)*NDOF+ij
          A(l) = CSR%A(pos)
        end do 
      end do

    end do
  end do
  CSR%N    = CSR%N*NDOF
  CSR%NP   = NP*NDOF2
  CSR%NDOF = 1
  deallocate(CSR%index)
  deallocate(CSR%item)
  deallocate(CSR%A)
  CSR%index => index
  CSR%item  => item
  CSR%A     => A

end subroutine imf_unblock_csr      

subroutine imf_convert_smat2fmat(S,F)

  use imf_util
  implicit none
  type (imf_smatrix) :: S
  type (imf_fmatrix) :: F
  integer(kind=kint) :: N,NP,NDOF,NDOF2
  integer(kind=kint) :: i,j,k,in,jS,jE,l
  integer(kind=kint),allocatable :: count(:), diff(:)

  N    = S%N
  NDOF = S%NDOF
  NDOF2= NDOF*NDOF
  NP   = S%NPL
  F%N   = N
  F%NDOF= NDOF
  F%NP  = NP
  allocate(F%AL(NP*NDOF2))
  F%D=>S%D
  F%symmetric=S%symmetric
  F%AU=>S%AU
  F%index=>S%indexU
  F%item=>S%itemU

  allocate( diff(N) )
  diff(:)=0
  do i = 1,N
    jS= S%indexL(i-1) + 1
    jE= S%indexL(i  )
    do j= jS, jE
      in = S%itemL(j)
    end do
  end do

  do i = 1,N
    jS= S%indexL(i-1) + 1
    jE= S%indexL(i  )
    do j= jS, jE
      in = S%itemL(j)
      do k = 1, NDOF
        do l = 1, NDOF
          F%AL( NDOF2*( F%index(in-1)+1+diff(in) ) -NDOF2+ (k-1)*NDOF+l ) = S%AL( NDOF2*(j-1) + (l-1)*NDOF+k )
        end do 
      end do

      diff(in) = diff(in) + 1
    end do
  end do

  deallocate(diff)

end subroutine imf_convert_smat2fmat

subroutine imf_convert_fmat2smat(F,S)

  use imf_util
  implicit none
  type (imf_smatrix) :: S
  type (imf_fmatrix) :: F
  integer(kind=kint) :: N,NP,NDOF,NDOF2
  integer(kind=kint) :: i,j,k,in,jS,jE,l
  integer(kind=kint),allocatable :: count(:), diff(:)

  N    = S%N
  NDOF = S%NDOF
  NDOF2= NDOF*NDOF
  NP   = S%NPL
  S%D=>F%D

  S%AU=F%AU
  allocate( diff(N) )
  diff(:)=0
  do i = 1,N
    jS= F%index(i-1) + 1
    jE= F%index(i  )
    do j= jS, jE
      in = F%item(j)
    end do
  end do

  do i = 1,N
    jS= F%index(i-1) + 1
    jE= F%index(i  )
    do j= jS, jE
      in = F%item(j)
      do k = 1, NDOF
        do l = 1, NDOF
          S%AL( NDOF2*( S%indexL(in-1)+1+diff(in) ) -NDOF2+ (k-1)*NDOF+l ) = F%AL( NDOF2*(j-1) + (l-1)*NDOF+k )
        end do 
      end do

      diff(in) = diff(in) + 1
    end do
  end do
  deallocate(diff)


end subroutine imf_convert_fmat2smat




