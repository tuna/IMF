
subroutine imf_substitution(A,X)
  use imf_util
  implicit none
  type (imf_smatrix) :: A
  real(kind=kreal) :: X(A%N*A%NDOF)
  integer(kind=kint) :: NDOF
  NDOF=A%NDOF
  if (NDOF == 1) then
    call imf_substitution_11(A,X)
  else if (NDOF == 3) then
    call imf_substitution_33(A,X)
  else
    call imf_substitution_nn(A,X)
  end if
end subroutine



subroutine imf_substitution_11(A,X)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  real(kind=kreal) :: X(A%N*A%NDOF)
  integer(kind=kint) :: i, j, iS, iE, k, N
  real(kind=kreal) :: SW(1)
  !-- FORWARD
  N=A%N
  do i = 1, N
    SW(1) = X(i)
    iS = A%indexL(i-1)+1
    iE = A%indexL(i)
    do j = iS, iE
      k  = A%itemL(j)
      SW(1) = SW(1) - A%AL(j)*X(k)
    enddo
    SW(1) =  SW(1) * A%D(i)
    X(i) = SW(1)
  enddo

  X = X / A%D 
  !C
  !C-- BACKWARD
  do i = N, 1, -1
    iS = A%indexU(i-1)+1
    iE = A%indexU(i)
    SW(1) = X(i)
    do j= iE, iS, -1
      k = A%itemU(j)
      SW(1)= SW(1) - A%AU(j)*X(k)
    enddo
    SW(1) = SW(1) * A%D(i)

    X(i) = SW(1)
  enddo
end subroutine

subroutine imf_substitution_33(A,X)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  real(kind=kreal) :: X(A%N*A%NDOF)
  integer(kind=kint) :: i, j, iS, iE, k, N
  real(kind=kreal) :: SW(3)
  !-- FORWARD
  N=A%N
  do i = 1, N
    SW(1) = X(3*i-2)
    SW(2) = X(3*i-1)
    SW(3) = X(3*i-0)
    iS = A%indexL(i-1)+1
    iE = A%indexL(i)
    do j = iS, iE
      k  = A%itemL(j)
      SW(1) = SW(1) - A%AL(9*j-8)*X(3*k-2) - A%AL(9*j-7)*X(3*k-1) - A%AL(9*j-6)*X(3*k-0)
      SW(2) = SW(2) - A%AL(9*j-5)*X(3*k-2) - A%AL(9*j-4)*X(3*k-1) - A%AL(9*j-3)*X(3*k-0)
      SW(3) = SW(3) - A%AL(9*j-2)*X(3*k-2) - A%AL(9*j-1)*X(3*k-1) - A%AL(9*j-0)*X(3*k-0)
    enddo
    SW(1) =  SW(1) * A%D(9*i-8)
    SW(2) = (SW(2) - A%D(9*i-5)*SW(1)) * A%D(9*i-4)
    SW(3) = (SW(3) - A%D(9*i-2)*SW(1) - A%D(9*i-1)*SW(2) ) * A%D(9*i-0)
    X(3*i-2) = SW(1)
    X(3*i-1) = SW(2)
    X(3*i-0) = SW(3)
  enddo

  do i = 1, N
    X(3*i-2) = X(3*i-2) / A%D(9*i-8)
    X(3*i-1) = X(3*i-1) / A%D(9*i-4)
    X(3*i-0) = X(3*i-0) / A%D(9*i-0)
  end do 
  !C
  !C-- BACKWARD
  do i = N, 1, -1
    iS = A%indexU(i-1)+1
    iE = A%indexU(i)
    SW(1) = X(3*i-2)
    SW(2) = X(3*i-1)
    SW(3) = X(3*i-0)
    do j= iE, iS, -1
      k = A%itemU(j)
      SW(3)= SW(3) - A%AU(9*j-2)*X(3*k-2) - A%AU(9*j-1)*X(3*k-1) - A%AU(9*j-0)*X(3*k  )
      SW(2)= SW(2) - A%AU(9*j-5)*X(3*k-2) - A%AU(9*j-4)*X(3*k-1) - A%AU(9*j-3)*X(3*k  )
      SW(1)= SW(1) - A%AU(9*j-8)*X(3*k-2) - A%AU(9*j-7)*X(3*k-1) - A%AU(9*j-6)*X(3*k  )
    enddo
    SW(3) = SW(3) * A%D(9*i-0)
    SW(2) = (SW(2) - A%D(9*i-3) * SW(3) ) * A%D(9*i-4)
    SW(1) = (SW(1) - A%D(9*i-7) * SW(2) - A%D(9*i-6) * SW(3) ) * A%D(9*i-8)

    X(3*i-2)=  SW(1)
    X(3*i-1)=  SW(2)
    X(3*i  )=  SW(3)
  enddo
end subroutine

subroutine imf_substitution_nn(A,X)

  use imf_util
  implicit none
  type (imf_smatrix) :: A
  real(kind=kreal) :: X(A%N*A%NDOF)
  integer(kind=kint) :: i, j, iS, iE, k, N, NDOF, NDOF2, idof, jdof
  real(kind=kreal) :: SW(A%NDOF)

  NDOF=A%NDOF
  NDOF2=NDOF*NDOF
  !-- FORWARD
  N=A%N
  do i = 1, N
    do idof = 1, NDOF
      SW(idof) = X(NDOF*(i-1)+idof)
    end do 
    iS = A%indexL(i-1)+1
    iE = A%indexL(i)
    do j = iS, iE
      k  = A%itemL(j)
      do idof = 1, NDOF
        do jdof = 1, NDOF
          SW(idof) = SW(idof) - A%AL(NDOF2*(j-1)+((idof-1)*NDOF)+jdof)*X(NDOF*(k-1)+jdof)
        end do 
      end do 
    enddo

    do idof = 1, NDOF
      do jdof = 1, idof-1
        SW(idof) = SW(idof) - A%D( NDOF2*(i-1)+ (idof-1)*NDOF+jdof ) * SW(jdof)
      end do 
      SW(idof) = SW(idof) * A%D(NDOF2*(i-1)+ idof*(NDOF+1)-NDOF)
    end do 

    do idof = 1, NDOF
      X(NDOF*(i-1)+idof) = SW(idof)
    end do 
  enddo

  do i = 1, N
    do idof = 1, NDOF
      X(NDOF*(i-1)+idof) = X(NDOF*(i-1)+idof) / A%D(NDOF2*(i-1)+ idof*(NDOF+1)-NDOF)
    end do 
  end do 
  !C
  !C-- BACKWARD
  do i = N, 1, -1
    iS = A%indexU(i-1)+1
    iE = A%indexU(i)
    do idof = 1, NDOF
      SW(idof) = X(NDOF*(i-1)+idof)
    end do 
    do j= iE, iS, -1
      k = A%itemU(j)
      do idof = 1, NDOF
        do jdof = 1, NDOF
          SW(idof) = SW(idof) - A%AU(NDOF2*(j-1)+((idof-1)*NDOF)+jdof)*X(NDOF*(k-1)+jdof)
        end do 
      end do 
    enddo

    do idof = NDOF, 1, -1
      do jdof = NDOF, idof+1, -1
        SW(idof) = SW(idof) - A%D( NDOF2*(i)- (NDOF-idof)*NDOF-(NDOF-jdof) ) * SW(jdof)
      end do 
      SW(idof) = SW(idof) * A%D(NDOF2*(i)- (NDOF-idof)*(NDOF+1))
    end do 

    do idof = 1, NDOF
      X(NDOF*(i-1)+idof) = SW(idof)
    end do 
  enddo

end subroutine
