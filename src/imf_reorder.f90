subroutine imf_reordering(A)
  use Metis5_API
  use, intrinsic  ::  iso_c_binding   !, only : C_int, C_double, C_float
  use imf_util
  implicit none
  type (imf_smatrix) :: A
  type (imf_dcsr),allocatable :: CSR
  integer(idx_t)                    ::  ierr
  integer(idx_t)                    ::  nvtxs
  integer(idx_t),pointer            ::  xadj(:)
  integer(idx_t),pointer            ::  adjncy(:)
  integer(idx_t),pointer            ::  vwgt(:)
  integer(idx_t),pointer            ::  options(:)
  integer(idx_t),pointer            ::  perm(:)
  integer(idx_t),pointer            ::  iperm(:)

  integer(kind=kint) :: i,j,k,N,jS,jE
  allocate(CSR)
  call imf_convert_smat2dcsr(A,CSR)

  N = A%N
  nvtxs = A%N

  allocate(A%perm(N))
  allocate(A%iperm(N))
  if (A%NPU==0) then
    do i = 1, N
      A%perm(i)=i
      A%iperm(i)=i
    end do
    return
  end if

  allocate(xadj(N+1))
  allocate(vwgt(N))
  allocate(adjncy(CSR%NP))

  do i = 0, N
    xadj(i+1) = CSR%index(i)
  end do
  do i = 1, N
    vwgt(i) = 1
  end do
  do i = 1, CSR%NP
    adjncy(i) = CSR%item(i) - 1
  end do
  allocate(perm(N))
  allocate(iperm(N))

  ierr = METIS_NodeND(nvtxs, xadj, adjncy, vwgt, options, perm, iperm)

  do i = 1, N
    A%perm(i)=perm(i)+1
    A%iperm(i)=iperm(i)+1
  end do
  !   do i = 1, N
  !     A%perm(i)=i
  !     A%iperm(i)=i
  !   end do
  deallocate(CSR)
  deallocate(perm)
  deallocate(iperm)
end subroutine

subroutine imf_matrix_reorder_profile(N,NPL,NPU, perm, iperm, &
    indexL, indexU, itemL, itemU, indexLp, indexUp, itemLp, itemUp)
  use imf_util

  implicit none
  integer(kind=kint), intent(in) :: N,NPL,NPU
  integer(kind=kint), intent(in) :: perm(N), iperm(N)
  integer(kind=kint), intent(in) :: indexL(0:N), indexU(0:N)
  integer(kind=kint), intent(in) :: itemL(NPL), itemU(NPU)
  integer(kind=kint), intent(out) :: indexLp(0:N), indexUp(0:N)
  integer(kind=kint), intent(out) :: itemLp(NPL), itemUp(NPU)
  integer(kind=kint) :: cntL, cntU, inew, iold, j, jold, jnew
  cntL = 0
  cntU = 0
  indexLp(0) = 0
  indexUp(0) = 0

  do inew=1,N
    iold = perm(inew)
    ! original L
    do j = indexL(iold-1)+1, indexL(iold)
      jold = itemL(j)
      jnew = iperm(jold)
      if (jnew < inew) then
        cntL = cntL + 1
        itemLp(cntL) = jnew
      else
        cntU = cntU + 1
        itemUp(cntU) = jnew
      end if
    end do
    ! original U
    do j = indexU(iold-1)+1, indexU(iold)
      jold = itemU(j)
      if (jold > N) cycle
      jnew = iperm(jold)
      if (jnew < inew) then
        cntL = cntL + 1
        itemLp(cntL) = jnew
      else
        cntU = cntU + 1
        itemUp(cntU) = jnew
      end if
    end do
    indexLp(inew) = cntL
    indexUp(inew) = cntU
    call quicksort_int_array(itemLp, indexLp(inew-1)+1, indexLp(inew))
    call quicksort_int_array(itemUp, indexUp(inew-1)+1, indexUp(inew))
  end do
end subroutine imf_matrix_reorder_profile


subroutine imf_matrix_reorder_values(N,NPL,NPU, NDOF, perm, iperm, &
    indexL, indexU, itemL, itemU, AL, AU, D, &
    indexLp, indexUp, itemLp, itemUp, ALp, AUp, Dp)
  use imf_util

  implicit none
  integer(kind=kint), intent(in) :: N,NPL,NPU, NDOF
  integer(kind=kint), intent(in) :: perm(N), iperm(N)
  integer(kind=kint), intent(in) :: indexL(0:N), indexU(0:N)
  integer(kind=kint), intent(in) :: itemL(NPL), itemU(NPU)
  real(kind=kreal), intent(in) :: AL(NPL*NDOF*NDOF), AU(NPU*NDOF*NDOF), D(N*NDOF*NDOF)
  integer(kind=kint), intent(in) :: indexLp(0:N), indexUp(0:N)
  integer(kind=kint), intent(in) :: itemLp(NPL), itemUp(NPU)
  real(kind=kreal), intent(out) :: ALp(NPL*NDOF*NDOF), AUp(NPU*NDOF*NDOF), Dp(N*NDOF*NDOF)
  Dp = 0.d0
  ALp = 0.d0
  AUp = 0.d0

  ! call reorder_diag(N, NDOF, perm, D, Dp)
  ! call reorder_off_diag(N, NDOF, perm, &
  !    indexL, indexU, itemL, itemU, AL, AU, &
  !    indexLp, itemLp, ALp)
  ! call reorder_off_diag(N, NDOF, perm, &
  !    indexL, indexU, itemL, itemU, AL, AU, &
  !    indexUp, itemUp, AUp)
  call reorder_diag2(N, NDOF, iperm, D, Dp)
  call reorder_off_diag2(N,NPL,NPU, NDOF, iperm, &
    indexL, itemL, AL, &
    indexLp, indexUp, itemLp, itemUp, ALp, AUp)
  call reorder_off_diag2(N,NPL,NPU, NDOF, iperm, &
    indexU, itemU, AU, &
    indexLp, indexUp, itemLp, itemUp, ALp, AUp)
end subroutine imf_matrix_reorder_values

subroutine reorder_diag2(N, NDOF, iperm, D, Dp)
  use imf_util

  implicit none
  integer(kind=kint), intent(in) :: N, NDOF
  integer(kind=kint), intent(in) :: iperm(N)
  real(kind=kreal), intent(in) :: D(N*NDOF*NDOF)
  real(kind=kreal), intent(out) :: Dp(N*NDOF*NDOF)
  integer(kind=kint) :: NDOF2, inew, iold, j0new, j0old, j
  NDOF2 = NDOF*NDOF
  ! diagonal
  !!$omp parallel default(none),private(iold,inew,j0old,j0new,j), &
  !!$omp&         shared(N,iperm,NDOF2,Dp,D)
  !!$omp do
  do iold=1,N
    inew = iperm(iold)
    j0old = (iold-1)*NDOF2
    j0new = (inew-1)*NDOF2
    do j=1,NDOF2
      Dp(j0new + j) = D(j0old + j)
    end do
  end do
  !!$omp end do
  !!$omp end parallel
end subroutine reorder_diag2    

subroutine reorder_off_diag2(N,NPL,NPU, NDOF, iperm, &
    indexX, itemX, AX, &
    indexLp, indexUp, itemLp, itemUp, ALp, AUp)
  use imf_util

  implicit none
  integer(kind=kint), intent(in) :: N,NPL,NPU,NDOF
  integer(kind=kint), intent(in) :: iperm(N)
  integer(kind=kint), intent(in) :: indexX(0:N)
  integer(kind=kint), intent(in) :: itemX(NPL)
  real(kind=kreal), intent(in) :: AX(NPL*NDOF*NDOF)
  integer(kind=kint), intent(in) :: indexLp(0:N), indexUp(0:N)
  integer(kind=kint), intent(in) :: itemLp(NPL), itemUp(NPU)
  real(kind=kreal), intent(out) :: ALp(NPL*NDOF*NDOF), AUp(NPU*NDOF*NDOF)
  integer(kind=kint) :: NDOF2, iold, inew
  integer(kind=kint) :: jsnewL, jenewL, jsnewU, jenewU
  integer(kind=kint) :: jold, kold, knew, jnew, l0old, l0new, l
  NDOF2 = NDOF*NDOF
  ! new L
  !!$omp parallel default(none), &
  !!$omp   private(iold,inew,jsnewL,jenewL,jsnewU,jenewU,jold,kold,knew,jnew,l0old,l0new,l), &
  !!$omp   shared(N,iperm,indexLp,indexUp,indexX,itemX,itemLp,NDOF2,ALp,AX,itemUp,AUp)
  !!$omp do
  do iold=1,N
    inew = iperm(iold)
    !write(*,*)iold,"=>",inew
    jsnewL = indexLp(inew-1)+1
    jenewL = indexLp(inew)
    jsnewU = indexUp(inew-1)+1
    jenewU = indexUp(inew)
    do jold = indexX(iold-1)+1, indexX(iold)
      kold = itemX(jold)
      if (kold > N) cycle
      knew = iperm(kold)
      if (knew < inew) then
        !write(*,*)itemLp(jsnewL:jenewL),jsnewL,jenewL,knew
        call bsearch_int_array(itemLp, jsnewL, jenewL, knew, jnew)
        if (jnew < 0) then
          write(0,*) 'ERROR:: jnew < 0 of L in reorder_off_diag2'
        end if
        l0old = (jold-1)*NDOF2
        l0new = (jnew-1)*NDOF2
        do l=1,NDOF2
          ALp(l0new + l) = AX(l0old + l)
        end do
      else
        !write(*,*)"itemUp",itemUp
        !write(*,*)"jsnewU",jsnewU
        !write(*,*)"jenewU",jenewU
        !write(*,*)"knew",knew
        call bsearch_int_array(itemUp, jsnewU, jenewU, knew, jnew)
        !write(*,*)jnew,knew,iold,jold

        if (jnew < 0) then
          write(0,*) 'ERROR:: jnew < 0 of U in reorder_off_diag2',jnew
        end if
        l0old = (jold-1)*NDOF2
        l0new = (jnew-1)*NDOF2
        do l=1,NDOF2
          AUp(l0new + l) = AX(l0old + l)
        end do
      end if
    end do
  end do
  !!$omp end do
  !!$omp end parallel
end subroutine reorder_off_diag2

subroutine imf_matrix_reorder_vector(N, NDOF, perm, X, Xp)
  use imf_util
  implicit none
  integer(kind=kint), intent(in) :: N, NDOF
  integer(kind=kint), intent(in) :: perm(N)
  real(kind=kreal), intent(in) :: X(N*NDOF)
  real(kind=kreal), intent(out) :: Xp(N*NDOF)
  integer(kind=kint) :: inew, iold, j0new, j0old, j

  !$omp parallel default(none),private(inew,iold,j0new,j0old,j), &
  !$omp   shared(N,perm,NDOF,Xp,X)
  !$omp do
  do inew=1,N
    iold = perm(inew)
    j0new = (inew-1)*NDOF
    j0old = (iold-1)*NDOF
    do j=1,NDOF
      Xp(j0new + j) = X(j0old + j)
    end do
  end do
  !$omp end do
  !$omp end parallel
end subroutine imf_matrix_reorder_vector

subroutine imf_matrix_reorder_back_vector(N, NDOF, perm, Xp, X)
  use imf_util

  implicit none
  integer(kind=kint), intent(in) :: N, NDOF
  integer(kind=kint), intent(in) :: perm(N)
  real(kind=kreal), intent(in) :: Xp(N)
  real(kind=kreal), intent(out) :: X(N)
  integer(kind=kint) :: inew, iold, j0new, j0old, j
  !$omp parallel default(none),private(inew,iold,j0new,j0old,j), &
  !$omp&  shared(N,perm,NDOF,X,Xp)
  !$omp do
  do inew=1,N
    iold = perm(inew)
    j0new = (inew-1)*NDOF
    j0old = (iold-1)*NDOF
    do j=1,NDOF
      X(j0old + j) = Xp(j0new + j)
    end do
  end do
  !$omp end do
  !$omp end parallel
end subroutine imf_matrix_reorder_back_vector

subroutine imf_permutation(A)
  use imf_util
  implicit none
  type (imf_smatrix) :: A

  integer(kind=kint) :: i,j,k,N,jS,jE
  integer(kind=kint) :: NDOF,NDOF2
  integer(kind=kint),pointer :: indexLp(:)
  integer(kind=kint),pointer :: indexUp(:)
  integer(kind=kint),pointer :: itemLp(:)
  integer(kind=kint),pointer :: itemUp(:)
  real(kind=kreal),pointer :: ALp(:)
  real(kind=kreal),pointer :: AUp(:)
  real(kind=kreal),pointer :: Dp(:)

  N=A%N
  NDOF=A%NDOF
  NDOF2=NDOF*NDOF
  allocate(indexLp(0:N))
  allocate(indexUp(0:N))
  allocate(itemLp(A%NPL))
  allocate(itemUp(A%NPU))
  allocate(ALp(A%NPL*NDOF2))
  allocate(AUp(A%NPU*NDOF2))
  allocate(Dp(N*NDOF2))

  !do i=1,N
  !  A%perm(i) = i
  !  A%iperm(i) = i
  !enddo
  call imf_matrix_reorder_profile(A%N,A%NPL,A%NPU, A%perm, A%iperm, &
    & A%indexL, A%indexU, A%itemL, A%itemU, indexLp, indexUp, itemLp, itemUp)

  call imf_matrix_reorder_values(A%N,A%NPL,A%NPU, A%NDOF, &
    & A%perm, A%iperm, A%indexL, A%indexU, A%itemL, A%itemU, A%AL, A%AU, A%D, &
    & indexLp, indexUp, itemLp, itemUp, ALp, AUp, Dp)

  deallocate(A%indexL)
  deallocate(A%indexU)
  deallocate(A%itemL)
  deallocate(A%itemU)
  deallocate(A%AL)
  deallocate(A%AU)
  deallocate(A%D)
  A%indexL => indexLp
  A%indexU => indexUp
  A%itemL => itemLp
  A%itemU => itemUp
  A%AL => ALp
  A%AU => AUp
  A%D => Dp

end subroutine imf_permutation


subroutine imf_permutation_vector(A,X)
  use imf_util
  implicit none
  type (imf_smatrix) :: A
  real(kind=kreal) :: X(A%NDOF*A%N)
  real(kind=kreal),allocatable :: Xp(:)
  integer(kind=kint) :: i,j,k,N,jS,jE,NDOF
  allocate(Xp(A%NDOF*A%N))

  call imf_matrix_reorder_vector(A%N, A%NDOF, A%perm, X, Xp)

  X=Xp
  deallocate(Xp)
end subroutine imf_permutation_vector


subroutine imf_inverse_permutation_vector(A,X)
  use imf_util
  implicit none
  type (imf_smatrix) :: A
  real(kind=kreal) :: X(A%NDOF*A%N)
  real(kind=kreal),allocatable :: Xp(:)
  integer(kind=kint) :: i,j,k,N,jS,jE,NDOF
  allocate(Xp(A%NDOF*A%N))

  call imf_matrix_reorder_vector(A%N, A%NDOF, A%iperm, X, Xp)

  X=Xp
  deallocate(Xp)
end subroutine imf_inverse_permutation_vector
