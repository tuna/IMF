

subdirs := src
.PHONY: all $(subdirs)
all: $(subdirs)
$(subdirs):
	$(MAKE) -C $@
test: $(subdirs)
	[ ! -e test ] && mkdir test;cd test;../bin/imf
clean:
	cd src;	make clean

 
