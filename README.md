IMF matrix library  
 -  LU factorization  
 - (Multi)frontal direct solver  
 -  Matrix function library 

imf_ctrl.dat:  
 - Matrix filepath  
 - Matrix type {CSR,BSR,COO}  
 - symmertic or asymmetric  

> ../matrix/test.coo  
> COO  
> asymmetric  


COO format:

>  5  5  8  
>    1     1   1.000e+00  
>    2     2   1.050e+01  
>    3     3   1.500e-02  
>    1     4   6.000e+00  
>    4     2   2.505e+02  
>    4     4  -2.800e+02  
>    4     5   3.332e+01  
>    5     5   1.200e+01  

CSR format:
> 
>


